package quota

import (
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
)

const (
	ResourceRequestsNvidiaGpu v1.ResourceName = "requests.nvidia.com/gpu"
	ResourceLimitsNvidiaGpu   v1.ResourceName = "limits.nvidia.com/gpu"
	ResourceRequestsAmdGpu    v1.ResourceName = "requests.amd.com/gpu"
	ResourceLimitsAmdGpu      v1.ResourceName = "limits.amd.com/gpu"
	ResourceNvidiaGpu         v1.ResourceName = "nvidia.com/gpu"
	ResourceAmdGpu            v1.ResourceName = "amd.com/gpu"
)

func NvidiaGpu(res v1.ResourceList) *resource.Quantity {
	if val, ok := res[ResourceNvidiaGpu]; ok {
		return &val
	}
	return &resource.Quantity{}
}
func AmdGpu(res v1.ResourceList) *resource.Quantity {
	if val, ok := res[ResourceAmdGpu]; ok {
		return &val
	}
	return &resource.Quantity{}
}
func NvidiaGpu2(res map[v1.ResourceName]*resource.Quantity) *resource.Quantity {
	if val, ok := res[ResourceNvidiaGpu]; ok {
		return val
	}
	return &resource.Quantity{}
}
func AmdGpu2(res map[v1.ResourceName]*resource.Quantity) *resource.Quantity {
	if val, ok := res[ResourceAmdGpu]; ok {
		return val
	}
	return &resource.Quantity{}
}
