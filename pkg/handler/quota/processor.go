package quota

import (
	"alauda.io/aml-api/pkg/handler/metrics"
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"fmt"
	"github.com/emicklei/go-restful"
	"github.com/juju/errors"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"strings"
)

type Processor struct {
	req *restful.Request
	mgr client.Manager
}

// NewProcessor constructor for internal Processor
func NewProcessor(mgr client.Manager, req *restful.Request) Processor {
	return Processor{req, mgr}
}

// retrieve all from prometheus when gpu exporter ready
func (p Processor) RetrieveAvailableResourceByNode(regionK8sClient kubernetes.Interface, nsQuery *NamespaceQuery) (ClusterResource, error) {
	var err error
	clusterResource := ClusterResource{}

	var clusterCpu, clusterFreeCpu, clusterMemory, clusterFreeMemory int64
	var clusterNvidiaGpu, clusterAmdGpu, clusterFreeNvidiaGpu int64
	var clusterVcudaCore, clusterVcudaMemory int64
	var clusterFreeVcudaCore, clusterFreeVcudaMemory int64
	cluster := p.req.PathParameter("cluster")
	nodeReqCPU, err := getMetricsProcessor(p).GetNodeReqCPU(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	nodeResourceMap := make(map[string]*NodeResource)
	for _, value := range nodeReqCPU {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			nodeResourceOutput = &NodeResource{}
			nodeResourceOutput.Name = string(value.Metric["node"])
			nodeResourceMap[nodeResourceOutput.Name] = nodeResourceOutput
		}
		nodeResourceOutput.RequestsCPU = int64(value.Value * 1000)
	}

	nodeAllCPU, err := getMetricsProcessor(p).GetNodeAllCPU(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeAllCPU {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.CPU = int64(value.Value * 1000)
		nodeResourceOutput.FreeCPU = nodeResourceOutput.CPU - nodeResourceOutput.RequestsCPU
		clusterCpu += nodeResourceOutput.CPU
		clusterFreeCpu += nodeResourceOutput.FreeCPU
	}

	// ------memory start ------
	nodeReqMem, err := getMetricsProcessor(p).GetNodeReqMem(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeReqMem {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.RequestsMemory = int64(value.Value)
	}

	nodeAllMem, err := getMetricsProcessor(p).GetNodeAllMem(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeAllMem {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.Memory = int64(value.Value)
		nodeResourceOutput.FreeMemory = nodeResourceOutput.Memory - nodeResourceOutput.RequestsMemory
		clusterMemory += nodeResourceOutput.Memory
		clusterFreeMemory += nodeResourceOutput.FreeMemory
	}
	// ------memory start ------

	// ------nvidia gpu start ------
	nodeReqNvidiaGpu, err := getMetricsProcessor(p).GetNodeReqNvidiaGpu(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeReqNvidiaGpu {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.RequestsNvidiaGPU = int64(value.Value)
	}

	nodeAllNvidiaGpu, err := getMetricsProcessor(p).GetNodeAllNvidiaGpu(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeAllNvidiaGpu {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.NvidiaGPU = int64(value.Value)
		nodeResourceOutput.FreeNvidiaGPU = nodeResourceOutput.NvidiaGPU - nodeResourceOutput.RequestsNvidiaGPU
		clusterNvidiaGpu += nodeResourceOutput.NvidiaGPU
		clusterFreeNvidiaGpu += nodeResourceOutput.FreeNvidiaGPU
	}
	// ------nvidia gpu end ------

	// ------vcuda-core gpu start ------
	nodeReqVcudaCoreGpu, err := getMetricsProcessor(p).GetNodeReqVCudaCore(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeReqVcudaCoreGpu {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.RequestsVCudaCore = int64(value.Value)
	}

	nodeAllVcudaCore, err := getMetricsProcessor(p).GetNodeAllVCudaCore(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeAllVcudaCore {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.VCudaCore = int64(value.Value)
		nodeResourceOutput.FreeVcudaCore = nodeResourceOutput.VCudaCore - nodeResourceOutput.RequestsVCudaCore
		clusterVcudaCore += nodeResourceOutput.VCudaCore
		clusterFreeVcudaCore += nodeResourceOutput.FreeVcudaCore
	}
	// ------vcuda core end -----

	// ------vcuda-core memory start ------
	nodeReqVcudaMemory, err := getMetricsProcessor(p).GetNodeReqVcudaMemory(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeReqVcudaMemory {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.RequestsVCudaMemory = int64(value.Value)
	}

	nodeAllVcudaMemory, err := getMetricsProcessor(p).GetNodeAllVCudaMemory(cluster)
	if err != nil {
		return clusterResource, errors.Trace(err)
	}
	for _, value := range nodeAllVcudaMemory {
		nodeResourceOutput, ok := nodeResourceMap[string(value.Metric["node"])]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", string(value.Metric["node"]))
		}
		nodeResourceOutput.VCudaMemory = int64(value.Value)
		nodeResourceOutput.FreeVCudaMemory = nodeResourceOutput.VCudaMemory - nodeResourceOutput.RequestsVCudaMemory
		clusterVcudaMemory += nodeResourceOutput.VCudaMemory
		clusterFreeVcudaMemory += nodeResourceOutput.FreeVCudaMemory
	}
	// ------vcuda core end -----

	// ------amd gpu start ------
	var nodeResource = make(map[string]v1.ResourceList)
	channels := ResourceChannels{
		NodeList: getNodeListChannel(regionK8sClient),
	}
	err = <-channels.NodeList.Error
	if err != nil {
		return clusterResource, err
	}
	nodeList := <-channels.NodeList.List
	for _, node := range nodeList.Items {
		if node.Spec.Unschedulable {
			continue
		}
		nodeResource[node.Name] = make(v1.ResourceList)

		//nodeResource[node.Name][v1.ResourceCPU] = *node.Status.Allocatable.Cpu()
		//nodeResource[node.Name][v1.ResourceMemory] = *node.Status.Allocatable.Memory()
		//nodeResource[node.Name][ResourceNvidiaGpu] = *NvidiaGpu(node.Status.Allocatable)
		nodeResource[node.Name][ResourceAmdGpu] = *AmdGpu(node.Status.Allocatable)
	}
	for nodeName, resMap := range nodeResource {
		nodeResourceOutput, ok := nodeResourceMap[nodeName]
		if !ok {
			return clusterResource, fmt.Errorf("unknown node %s \n", nodeName)
		}
		nodeResourceOutput.AmdGPU, _ = AmdGpu(resMap).AsInt64()
		nodeResourceOutput.FreeAmdGPU, _ = AmdGpu(resMap).AsInt64()
		clusterAmdGpu += nodeResourceOutput.AmdGPU
	}
	// ------amd gpu end ------

	clusterResource.CPU = clusterCpu
	clusterResource.Memory = clusterMemory

	clusterResource.FreeCPU = clusterFreeCpu
	clusterResource.FreeMemory = clusterFreeMemory

	clusterResource.NvidiaGPU = clusterNvidiaGpu
	clusterResource.FreeNvidiaGPU = clusterFreeNvidiaGpu

	clusterResource.AmdGPU = clusterAmdGpu
	clusterResource.FreeAmdGPU = clusterAmdGpu

	clusterResource.VCudaMemory = clusterVcudaMemory
	clusterResource.FreeVcudaMemory = clusterFreeVcudaMemory

	clusterResource.VCudaCore = clusterVcudaCore
	clusterResource.FreeVcudaCore = clusterFreeVcudaCore

	clusterResource.NodeResource = mapToSlice(nodeResourceMap)

	return clusterResource, nil
}

func mapToSlice(m map[string]*NodeResource) []NodeResource {
	result := make([]NodeResource, 0, len(m))
	for _, value := range m {
		result = append(result, *value)
	}
	return result
}

func getContainersRequestAndLimit(containers []v1.Container) (reqs map[v1.ResourceName]resource.Quantity, limits map[v1.ResourceName]resource.Quantity) {
	reqs, limits = map[v1.ResourceName]resource.Quantity{}, map[v1.ResourceName]resource.Quantity{}
	for _, container := range containers {
		for name, quantity := range container.Resources.Requests {
			value, ok := reqs[name]
			if !ok {
				reqs[name] = resource.Quantity{}
			}
			value.Add(quantity)
			reqs[name] = value
		}
		for name, quantity := range container.Resources.Limits {
			value, ok := limits[name]
			if !ok {
				limits[name] = *quantity.Copy()
			}
			value.Add(quantity)
			limits[name] = value
		}
	}
	return reqs, limits
}

func PodRequestsAndLimits(pod *v1.Pod) (reqs v1.ResourceList, limits v1.ResourceList) {
	reqs, limits = getContainersRequestAndLimit(pod.Spec.Containers)
	// init containers define the minimum of any resource
	for _, container := range pod.Spec.InitContainers {
		for name, quantity := range container.Resources.Requests {
			value, _ := reqs[name]
			if quantity.Cmp(value) > 0 {
				reqs[name] = *quantity.Copy()
			}
		}
		for name, quantity := range container.Resources.Limits {
			value, _ := limits[name]
			if quantity.Cmp(value) > 0 {
				limits[name] = *quantity.Copy()
			}
		}
	}
	return
}

const (
	Prefix = "requests."
)

func setValue(namespaceQuota *NamespaceQuota, resQuotaStatus v1.ResourceQuotaStatus, valueType string, name, toName v1.ResourceName) {
	hard := resQuotaStatus.Hard[name]
	used := resQuotaStatus.Used[name]
	var vHard, vUsed int64
	if valueType == "MilliValue" {
		vHard = hard.MilliValue()
		vUsed = used.MilliValue()
	}
	if valueType == "Int64" {
		vHard, _ = hard.AsInt64()
		vUsed, _ = used.AsInt64()
	}
	namespaceQuota.Used[name] = fmt.Sprintf("%v", vUsed)
	namespaceQuota.Hard[name] = fmt.Sprintf("%v", vHard)
	if strings.HasPrefix(name.String(), Prefix) {
		namespaceQuota.Free[toName] = fmt.Sprintf("%v", vHard-vUsed)
	}
}
func (p Processor) RetrieveResourceQuota(regionK8sClient kubernetes.Interface, namespace, name string) (bool, NamespaceQuota, error) {
	namespaceQuota := NamespaceQuota{
		Hard: make(ResourceValue),
		Used: make(ResourceValue),
		Free: make(ResourceValue),
	}
	resQuota, err := regionK8sClient.CoreV1().ResourceQuotas(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return false, namespaceQuota, err
	}
	if resQuota.Status.Used != nil {
		for k := range resQuota.Status.Used {
			switch k {
			case v1.ResourceRequestsMemory, v1.ResourceLimitsMemory:
				setValue(&namespaceQuota, resQuota.Status, "Int64", k, v1.ResourceMemory)
			case ResourceRequestsNvidiaGpu, ResourceLimitsNvidiaGpu:
				setValue(&namespaceQuota, resQuota.Status, "Int64", k, ResourceNvidiaGpu)
			case ResourceRequestsAmdGpu, ResourceLimitsAmdGpu:
				setValue(&namespaceQuota, resQuota.Status, "Int64", k, ResourceAmdGpu)
			case v1.ResourceRequestsCPU, v1.ResourceLimitsCPU:
				setValue(&namespaceQuota, resQuota.Status, "MilliValue", k, v1.ResourceCPU)
			}
		}
		return true, namespaceQuota, nil
	}
	return false, namespaceQuota, nil
}

func getMetricsProcessor(p Processor) metrics.Processor {
	return metrics.NewProcessor(p.mgr, p.req)
}
