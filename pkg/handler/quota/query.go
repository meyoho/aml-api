package quota

import (
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type NamespaceQuery struct {
	namespaces []string
}

// NewSameNamespaceQuery creates new namespace query that queries single namespace.
func NewSameNamespaceQuery(namespace string) *NamespaceQuery {
	return &NamespaceQuery{[]string{namespace}}
}

// NewNamespaceQuery creates new query for given namespaces.
func NewNamespaceQuery(namespaces []string) *NamespaceQuery {
	return &NamespaceQuery{namespaces}
}

// ToRequestParam returns K8s API namespace query for list of objects from this namespaces.
// This is an optimization to query for single namespace if one was selected and for all
// namespaces otherwise.
func (n *NamespaceQuery) ToRequestParam() string {
	if len(n.namespaces) == 1 {
		return n.namespaces[0]
	}
	return v1.NamespaceAll
}

// Matches returns true when the given namespace matches this query.
func (n *NamespaceQuery) Matches(namespace string) bool {
	if len(n.namespaces) == 0 {
		return true
	}

	for _, queryNamespace := range n.namespaces {
		if namespace == queryNamespace {
			return true
		}
	}
	return false
}

//---------
type NodeListChannel struct {
	List  chan *v1.NodeList
	Error chan error
}
type PodListChannel struct {
	List  chan *v1.PodList
	Error chan error
}

type ResourceChannels struct {
	NodeList NodeListChannel
}

func getPodListChannel(regionK8sClient kubernetes.Interface, nsQuery *NamespaceQuery) PodListChannel {
	podListChannel := PodListChannel{
		List:  make(chan *v1.PodList, 1),
		Error: make(chan error, 1),
	}
	go func() {
		pods, err := regionK8sClient.CoreV1().Pods(nsQuery.ToRequestParam()).List(metav1.ListOptions{})
		podListChannel.List <- pods
		podListChannel.Error <- err

	}()
	return podListChannel
}

func getNodeListChannel(regionK8sClient kubernetes.Interface) NodeListChannel {
	nodeListChannel := NodeListChannel{
		List:  make(chan *v1.NodeList, 1),
		Error: make(chan error, 1),
	}
	go func() {
		pods, err := regionK8sClient.CoreV1().Nodes().List(metav1.ListOptions{})
		nodeListChannel.List <- pods
		nodeListChannel.Error <- err

	}()
	return nodeListChannel
}
