package quota

import (
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
}

func NewHandler(svr server.Server) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
	}
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/quota/remaining")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "modeldeploies")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AMLClientFilter).
				Doc("get remaining quota of namespace").
				Param(ws.PathParameter("cluster", "cluster to retrieve")).
				Param(ws.PathParameter("namespace", "namespace to retrieve")).
				To(handler.RetrieveNamespacedQuota).
				Returns(http.StatusOK, "ok", ResourceQuota{}),
		),
	)
	return
}
func (h Handler) RetrieveNamespacedQuota(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.QueryParameter("name")
	needDrillDown := req.QueryParameter("drill")
	if name == "" {
		name = "default"
	}
	regionK8sClient := abctx.Client(req.Request.Context())
	resourceQuota := ResourceQuota{}
	quotaEnabled, quota, err := h.getProcessor(req).RetrieveResourceQuota(regionK8sClient, namespace, name)
	if err != nil {
		h.WriteResponse(resourceQuota, err, req, resp)
		return
	}
	if quotaEnabled {
		resourceQuota.NamespaceQuota = quota
	}
	if !quotaEnabled || len(needDrillDown) > 0 {
		clusterResource, err := h.getProcessor(req).RetrieveAvailableResourceByNode(regionK8sClient, &NamespaceQuery{})
		if err != nil {
			h.WriteResponse(resourceQuota, err, req, resp)
			return
		}
		resourceQuota.ClusterResource = clusterResource
	}
	h.WriteResponse(resourceQuota, err, req, resp)
	return
}

func (h Handler) getProcessor(req *restful.Request) Processor {
	return NewProcessor(h.Server.GetManager(), req)
}
