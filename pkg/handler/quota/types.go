package quota

import (
	"k8s.io/api/core/v1"
)

type ResourceQuota struct {
	NamespaceQuota  NamespaceQuota  `json:"namespaceQuota"`
	ClusterResource ClusterResource `json:"clusterResource"`
}
type ResourceValue map[v1.ResourceName]string
type NamespaceQuota struct {
	// +optional
	Hard ResourceValue `json:"hard,omitempty"`
	// Used is the current observed total usage of the resource in the namespace.
	// +optional
	Used ResourceValue `json:"used,omitempty"`
	// +optional
	Free ResourceValue `json:"free,omitempty"`
}

type ClusterResource struct {
	CPU             int64          `json:"total.cpu,omitempty"`
	Memory          int64          `json:"total.memory,omitempty"`
	FreeCPU         int64          `json:"free.cpu,omitempty"`
	FreeMemory      int64          `json:"free.memory,omitempty"`
	NvidiaGPU       int64          `json:"total.nvidia_gpu,omitempty"`
	FreeNvidiaGPU   int64          `json:"free.nvidia_gpu,omitempty"`
	AmdGPU          int64          `json:"total.amd_gpu,omitempty"`
	FreeAmdGPU      int64          `json:"free.amd_gpu,omitempty"`
	VCudaCore       int64          `json:"total.vcuda_core,omitempty"`
	FreeVcudaCore   int64          `json:"free.vcuda_core,omitempty"`
	VCudaMemory     int64          `json:"total.vcuda_memory,omitempty"`
	FreeVcudaMemory int64          `json:"free.vcuda_memory,omitempty"`
	NodeResource    []NodeResource `json:"nodeResource,omitempty"`
}
type NodeResource struct {
	Name                string `json:"name"`
	CPU                 int64  `json:"total.cpu"`
	Memory              int64  `json:"total.memory"`
	RequestsCPU         int64  `json:"requests.cpu"`
	RequestsMemory      int64  `json:"requests.memory"`
	FreeCPU             int64  `json:"free.cpu"`
	FreeMemory          int64  `json:"free.memory"`
	NvidiaGPU           int64  `json:"total.nvidia_gpu,omitempty"`
	RequestsNvidiaGPU   int64  `json:"requests.nvidia_gpu,omitempty"`
	FreeNvidiaGPU       int64  `json:"free.nvidia_gpu,omitempty"`
	VCudaCore           int64  `json:"total.vcuda_core,omitempty"`
	RequestsVCudaCore   int64  `json:"requests.vcuda_core,omitempty"`
	FreeVcudaCore       int64  `json:"free.vcuda_core,omitempty"`
	VCudaMemory         int64  `json:"total.vcuda_memory,omitempty"`
	RequestsVCudaMemory int64  `json:"requests.vcuda_memory,omitempty"`
	FreeVCudaMemory     int64  `json:"free.vcuda_memory,omitempty"`
	AmdGPU              int64  `json:"total.amd_gpu,omitempty"`
	RequestsAmdGPU      int64  `json:"requests.amd_gpu,omitempty"`
	FreeAmdGPU          int64  `json:"free.amd_gpu,omitempty"`
}
