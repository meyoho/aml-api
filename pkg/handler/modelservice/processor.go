package modelservice

import (
	"alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/client-go/dynamic"
)

type processor struct {
	server server.Server
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//
func (p processor) ListModelService(ctx context.Context, client dynamic.NamespaceableResourceInterface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *ApplicationList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list Model Service end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   common.GetLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}

	list, err := client.Namespace(namespace.ToRequestParam()).List(listOptions)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data = &ApplicationList{
		Items:    make([]metav1.Object, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = result
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

func (p processor) RetrieveModelService(ctx context.Context, client dynamic.NamespaceableResourceInterface, namespace, name string) (*unstructured.Unstructured, error) {
	ins, err := client.Namespace(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &unstructured.Unstructured{}, err
	}
	return ins, nil
}
func (p processor) DeleteModelService(ctx context.Context, client dynamic.NamespaceableResourceInterface, namespace, name string) error {
	var err error
	err = client.Namespace(namespace).Delete(name, &metav1.DeleteOptions{})
	if err != nil && !errors.IsNotFoundError(err) {
		return err
	}
	return nil
}
