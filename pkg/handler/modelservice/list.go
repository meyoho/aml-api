package modelservice

import (
	"alauda.io/aml-api/pkg/handler/common"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// PipelineList contains a list of jenkins in the cluster.
type ApplicationList struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of Pipeline.
	Items []metav1.Object `json:"applications"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}
