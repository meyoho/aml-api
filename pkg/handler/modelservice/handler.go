package modelservice

import (
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"github.com/kubernetes-sigs/application/pkg/apis/app/v1beta1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"net/http"
)

var gvkApp = &schema.GroupVersionKind{
	Group:   "app.k8s.io",
	Kind:    "Application",
	Version: "v1beta1",
}

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	RetrieveModelService(ctx context.Context, client dynamic.NamespaceableResourceInterface, namespace, name string) (*unstructured.Unstructured, error)
	DeleteModelService(ctx context.Context, client dynamic.NamespaceableResourceInterface, namespace, modelDeployName string) error
	ListModelService(ctx context.Context, client dynamic.NamespaceableResourceInterface, query *dataselect.Query, namespace *common.NamespaceQuery) (*ApplicationList, error)
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/modelservices")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "modeldeployments")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{cluster}/{namespace}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List Notebook instance`).
					Param(ws.PathParameter("cluster", "cluster of the model service")).
					Param(ws.PathParameter("namespace", "namespace of the model service")).
					To(handler.ListModelService).
					Writes(ApplicationList{}).
					Returns(http.StatusOK, "OK", ApplicationList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a model service").
				Param(ws.PathParameter("cluster", "cluster of the model service")).
				Param(ws.PathParameter("namespace", "namespace of the model service")).
				Param(ws.PathParameter("name", "name of the model service")).
				To(handler.RetrieveModelService).
				Returns(http.StatusOK, "ok", v1beta1.Application{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a model service").
				Param(ws.PathParameter("cluster", "cluster of the model service")).
				Param(ws.PathParameter("namespace", "namespace of the model service")).
				Param(ws.PathParameter("name", "name of the model service")).
				To(handler.DeleteModelService).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}
func (h Handler) RetrieveModelService(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	dynClient, err := h.Server.GetManager().DynamicClient(req, gvkApp)
	if err != nil {
		return
	}
	notebook, err := h.Processor.RetrieveModelService(req.Request.Context(), dynClient, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) DeleteModelService(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	dynClient, err := h.Server.GetManager().DynamicClient(req, gvkApp)
	if err != nil {
		return
	}
	err = h.Processor.DeleteModelService(req.Request.Context(), dynClient, namespace, name)
}
func (h Handler) ListModelService(req *restful.Request, resp *restful.Response) {
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	query := abctx.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	dynClient, err := h.Server.GetManager().DynamicClient(req, gvkApp)
	if err != nil {
		return
	}
	list, err := h.Processor.ListModelService(req.Request.Context(), dynClient, query, namespace)
	h.WriteResponse(list, err, req, resp)
}
