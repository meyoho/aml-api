package system

import (
	"alauda.io/aml-api/pkg/handler/common/env"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"sigs.k8s.io/yaml"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//
func (p processor) RetrieveSystemImages(ctx context.Context, client kubernetes.Interface, namespace, name string) (*SystemImagesList, error) {
	apiConfigMap, err := client.CoreV1().ConfigMaps(namespace).Get(name, metav1.GetOptions{})
	imgList := &SystemImagesList{}
	if err != nil {
		return imgList, err
	}
	images := &[]SystemImage{}
	privateImageKey := env.GetEnv(env.CMPrivateImagesKey, "private-images")
	imageStr := apiConfigMap.Data[privateImageKey]
	privateRegistryKey := env.GetEnv(env.CMPrivateRegistryKey, "private-registry")
	registryStr := apiConfigMap.Data[privateRegistryKey]
	err = yaml.Unmarshal([]byte(imageStr), images)
	if err != nil {
		return imgList, err
	}
	privateScopeKey := env.GetEnv(env.CMPrivateImagesScopeKey, "private-images-scope")
	privateImageScopeStr := apiConfigMap.Data[privateScopeKey]
	scopes := &[]struct {
		Scope  string
		Choice string
	}{}
	err = yaml.Unmarshal([]byte(privateImageScopeStr), scopes)
	if err != nil {
		return imgList, err
	}
	scopeMap := make(map[string]string)
	for _, v := range *scopes {
		scopeMap[v.Scope] = v.Choice
	}
	imgList.Registry = registryStr
	mapSystemImageTags := make(map[string]*SystemImageTags)
	for _, v := range *images {
		if scopeMap[v.Scope] == "true" {
			if _, ok := mapSystemImageTags[v.Image]; !ok {
				mapSystemImageTags[v.Image] = &SystemImageTags{Image: v.Image, Tags: []string{v.Tag}, Type: v.Type}
				continue
			}
			if !contains(mapSystemImageTags[v.Image].Tags, v.Tag) {
				mapSystemImageTags[v.Image].Tags = append(mapSystemImageTags[v.Image].Tags, v.Tag)
			}
		}
	}
	for _, v := range mapSystemImageTags {
		imgList.Items = append(imgList.Items, *v)
	}
	imgList.ListMeta.TotalItems = len(*images)
	return imgList, nil
}

func contains(slice []string, str string) bool {
	for _, v := range slice {
		if v == str {
			return true
		}
	}
	return false
}
