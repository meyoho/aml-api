package system

import (
	"alauda.io/aml-api/pkg/handler/common"
)

type SystemImage struct {
	Image string `json:"image"`
	Tag   string `json:"tag"`
	Type  string `json:"type"`
	Scope string `json:"scope"`
}
type SystemImageTags struct {
	Image string
	Tags  []string
	Type  string
}

type SystemImagesList struct {
	ListMeta common.ListMeta `json:"listMeta"`
	// Unordered list of Pipeline.
	Registry string            `json:"registry"`
	Items    []SystemImageTags `json:"images"`
}
