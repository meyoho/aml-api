package system

import (
	"alauda.io/aml-api/pkg/handler/common"
	ctx "context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes/fake"
	"reflect"
)

var _ = Describe("System images", func() {
	var (
		systemImages *SystemImagesList
		err          error
		client       *fake.Clientset
	)
	BeforeEach(func() {
		client = fake.NewSimpleClientset(GetObjs())
		systemImages, err = NewProcessor().RetrieveSystemImages(ctx.Background(), client, "alauda-system", "aml-api")
	})
	It("should not got error", func() {
		Expect(err).NotTo(HaveOccurred())
	})
	It("get system images", func() {
		v1 := systemImages
		v2 := GetExpect()
		b := reflect.DeepEqual(v1, v2)
		Expect(true, b)
	})
})

func GetExpect() *SystemImagesList {
	return &SystemImagesList{
		ListMeta: common.ListMeta{TotalItems: 3},
		Registry: "index.alauda.cn",
		Items: []SystemImageTags{
			{
				Image: "alaudaorg/cat-prediction-web",
				Tags:  []string{"0.8.0", "0.8.1"},
				Type:  "example_modelservice_consumer",
			},
			{
				Image: "alaudaorg/tensorflow-1.13.1-notebook-cpu",
				Tags:  []string{"v0.5.9"},
				Type:  "notebook_tf",
			},
		},
	}
}
func GetObjs() *v1.ConfigMap {
	cm := &v1.ConfigMap{}
	cm.Name = "aml-api"
	cm.Namespace = "alauda-system"
	cm.Data = make(map[string]string)
	cm.Data["private-images"] = `
- image: alaudaorg/cat-prediction-web
  scope: basic
  tag: 0.8.0
  type: example_modelservice_consumer
- image: alaudaorg/cat-prediction-web
  scope: basic
  tag: 0.8.1
  type: example_modelservice_consumer
- image: alaudaorg/tensorflow-1.13.1-notebook-cpu
  scope: basic
  tag: v0.5.9
  type: notebook_tf`
	cm.Data["private-registry"] = "index.alauda.cn"
	cm.Data["private-images-scope"] = `
    - scope: basic
      choice: true
    - scope: demo
      choice: false
    - scope: full
      choice: false
`
	return cm
}
