package system

import (
	"alauda.io/aml-api/pkg/base"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/aml-api/pkg/handler/common/env"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	RetrieveSystemImages(ctx context.Context, client kubernetes.Interface, namespace, name string) (*SystemImagesList, error)
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/amlimages")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "system-images")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/system").
				Filter(clientFilter.AMLClientFilter).
				Doc("get all system images").
				To(handler.RetrieveSystemImages).
				Returns(http.StatusOK, "ok", SystemImagesList{}),
		),
	)
	ws.Filter(clientFilter.SecureFilter)
	return
}
func (h Handler) RetrieveSystemImages(req *restful.Request, resp *restful.Response) {
	client := abctx.Client(req.Request.Context())
	namespace := base.GetAlaudaNamespace()
	name := env.GetEnv(env.ConfigMapName, "aml-api")
	images, err := h.Processor.RetrieveSystemImages(req.Request.Context(), client, namespace, name)
	h.WriteResponse(images, err, req, resp)
	return
}
