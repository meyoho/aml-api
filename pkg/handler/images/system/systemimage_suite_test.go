package system

import (
	. "github.com/onsi/ginkgo"
	"github.com/onsi/ginkgo/reporters"
	. "github.com/onsi/gomega"
	"testing"
)

func TestSystemImage(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("system_image.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/handler/images/system", []Reporter{junitReporter})
}
