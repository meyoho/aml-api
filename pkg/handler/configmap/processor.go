package configmap

import (
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type Processor struct {
}

func (p Processor) GetConfigMap(client kubernetes.Interface, namespace, name string) (*v1.ConfigMap, error) {
	cfg, err := client.CoreV1().ConfigMaps(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1.ConfigMap{}, err
	}
	return cfg, nil
}

func (p Processor) CreateConfigMap(client kubernetes.Interface, namespace string, requestObj *v1.ConfigMap) (*v1.ConfigMap, error) {
	return client.CoreV1().ConfigMaps(namespace).Create(requestObj)
}

func (p Processor) UpdateConfigMap(client kubernetes.Interface, namespace, name string, requestObj *v1.ConfigMap) (*v1.ConfigMap, error) {
	fromObj, err := client.CoreV1().ConfigMaps(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1.ConfigMap{}, err
	}
	to := fromObj.DeepCopy()
	to.Data = requestObj.Data
	return client.CoreV1().ConfigMaps(namespace).Update(to)
}
