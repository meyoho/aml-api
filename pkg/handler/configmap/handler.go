package configmap

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/api/core/v1"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	processor Processor
}

func NewHandler(svr server.Server) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		processor:       Processor{},
	}
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/configmaps")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "configmaps")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get configmap").
				Param(ws.PathParameter("namespace", "namespace of the configmap")).
				Param(ws.PathParameter("name", "name of the configmap")).
				To(handler.GetConfigMap).
				Returns(http.StatusOK, "ok", v1.ConfigMap{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("create a configmap").
				Param(ws.PathParameter("namespace", "namespace of the configmap")).
				To(handler.CreateConfigMap).
				Returns(http.StatusOK, "ok", v1.ConfigMap{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update a configmap").
				Param(ws.PathParameter("namespace", "namespace of the configmap")).
				Param(ws.PathParameter("name", "name of the configmap")).
				To(handler.UpdateConfigMap).
				Returns(http.StatusOK, "ok", v1.ConfigMap{}),
		),
	)

	ws.Filter(clientFilter.AMLClientFilter)
	return
}

func (h Handler) GetConfigMap(req *restful.Request, resp *restful.Response) {
	client := localctx.InsecureClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.processor.GetConfigMap(client, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}

func (h Handler) CreateConfigMap(req *restful.Request, resp *restful.Response) {
	client := localctx.InsecureClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	var configMap *v1.ConfigMap
	var requestObj = &v1.ConfigMap{}
	var err error
	defer func() {
		h.WriteResponse(configMap, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	configMap, err = h.processor.CreateConfigMap(client, namespace, requestObj)
}

func (h Handler) UpdateConfigMap(req *restful.Request, resp *restful.Response) {
	client := localctx.InsecureClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var ppCfg *v1.ConfigMap
	var requestObj = &v1.ConfigMap{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.processor.UpdateConfigMap(client, namespace, name, requestObj)
}
