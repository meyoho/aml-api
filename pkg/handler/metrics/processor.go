package metrics

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"github.com/emicklei/go-restful"
	"github.com/juju/errors"
	"github.com/prometheus/common/model"
)

type Processor struct {
	req *restful.Request
	mgr client.Manager
}

// NewProcessor constructor for internal Processor
func NewProcessor(mgr client.Manager, req *restful.Request) Processor {
	return Processor{req, mgr}
}

func (p Processor) GetPodMem(clusterName string, namespace string,
	podName string) (int64, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return -1, errors.Trace(err)
	}
	return dyClient.QueryPodMEM(clusterName, namespace, podName)
}

func (p Processor) GetPodCPU(clusterName string, namespace string,
	podName string) (int64, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return -1, errors.Trace(err)
	}
	return dyClient.QueryPodCPU(clusterName, namespace, podName)
}

func (p Processor) GetPodNvidiaGpuUtilization(clusterName string, namespace string,
	podName string) (float64, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return -1, errors.Trace(err)
	}
	return dyClient.QueryPodNvidiaGpuUtilization(clusterName, namespace, podName)
}

func (p Processor) GetNodeReqMem(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeReqRes(clusterName, "kube_pod_container_resource_requests_memory_bytes")
}

func (p Processor) GetNodeReqCPU(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeReqRes(clusterName, "kube_pod_container_resource_requests_cpu_cores")
}

func (p Processor) GetNodeReqNvidiaGpu(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeReqRes(clusterName, "kube_pod_container_resource_limits_nvidia_gpu_devices")
}

func (p Processor) GetNodeReqVCudaCore(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeReqRes(clusterName, "kube_pod_container_resource_limits_tencent_gpu_cores")
}

func (p Processor) GetNodeReqVcudaMemory(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeReqRes(clusterName, "kube_pod_container_resource_limits_tencent_gpu_memory")
}

func (p Processor) GetNodeAllMem(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeAllRes(clusterName, "kube_node_status_allocatable_memory_bytes")
}

func (p Processor) GetNodeAllCPU(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeAllRes(clusterName, "kube_node_status_allocatable_cpu_cores")
}

func (p Processor) GetNodeAllNvidiaGpu(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeAllRes(clusterName, "kube_node_status_allocatable_nvidia_gpu_cards")
}
func (p Processor) GetNodeAllVCudaCore(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeAllRes(clusterName, "kube_node_status_allocatable_tencent_gpu_cores")
}

func (p Processor) GetNodeAllVCudaMemory(clusterName string) (model.Vector, error) {
	dyClient, err := getPbClient(p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	return dyClient.QueryNodeAllRes(clusterName, "kube_node_status_allocatable_tencent_gpu_memory")
}

func getPbClient(p Processor) (*MorgansClient, error) {
	return New(p.req)
}
