package metrics

import (
	"alauda.io/aml-api/cmd/aml-api/app/options"
	client2 "bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"encoding/json"
	"fmt"
	"github.com/emicklei/go-restful"
	juju "github.com/juju/errors"
	"github.com/prometheus/client_golang/api/prometheus/v1"
	"github.com/prometheus/common/model"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/api/errors"
	"net/http"
	"strings"
)

type MorgansClient struct {
	httpClient *http.Client
	req        *restful.Request
}

type apiResponse struct {
	Status    string       `json:"status"`
	Data      queryResult  `json:"data"`
	ErrorType v1.ErrorType `json:"errorType"`
	Error     string       `json:"error"`
}

func (qr *queryResult) UnmarshalJSON(b []byte) error {
	v := struct {
		Type   model.ValueType `json:"resultType"`
		Result json.RawMessage `json:"result"`
	}{}

	err := json.Unmarshal(b, &v)
	if err != nil {
		return err
	}

	switch v.Type {
	case model.ValScalar:
		var sv model.Scalar
		err = json.Unmarshal(v.Result, &sv)
		qr.v = &sv

	case model.ValVector:
		var vv model.Vector
		err = json.Unmarshal(v.Result, &vv)
		qr.v = vv

	case model.ValMatrix:
		var mv model.Matrix
		err = json.Unmarshal(v.Result, &mv)
		qr.v = mv

	default:
		err = fmt.Errorf("unexpected value type %q", v.Type)
	}
	return err
}

// queryResult contains result data for a query.
type queryResult struct {
	Type   model.ValueType `json:"resultType"`
	Result interface{}     `json:"result"`

	// The decoded value.
	v model.Value
}

func (c *MorgansClient) QueryPodMEM(clusterName string, namespace string, name string) (int64, error) {
	queryString := getPodMemQuery(namespace, name)
	vectors, err := c.Query(clusterName, queryString)
	if vectors.Len() != 1 {
		return -1, fmt.Errorf("incorrect len: %d\n", vectors.Len())
	}
	return int64(vectors[0].Value), juju.Trace(err)
}

func (c *MorgansClient) QueryPodNvidiaGpuUtilization(clusterName string, namespace string, name string) (float64, error) {
	queryString := getPodNvidiaGpuUtilization(namespace, name)
	vectors, err := c.Query(clusterName, queryString)
	if vectors.Len() != 1 {
		return -1, fmt.Errorf("incorrect len: %d\n", vectors.Len())
	}
	return float64(vectors[0].Value), juju.Trace(err)
}

func (c *MorgansClient) QueryNodeAllRes(clusterName string, resourceName string) (model.Vector, error) {
	queryString := getNodeAllResQuery(resourceName)
	result, err := c.Query(clusterName, queryString)
	return result, juju.Trace(err)
}

func (c *MorgansClient) QueryNodeReqRes(clusterName string, resourceName string) (model.Vector, error) {
	queryString := getNodeReqResQuery(resourceName)
	result, err := c.Query(clusterName, queryString)
	return result, juju.Trace(err)
}

func getNodeAllResQuery(resourceName string) string {
	memQueryString := `sum by(node) (%s) `
	return fmt.Sprintf(memQueryString, resourceName)
}

func getNodeReqResQuery(resourceName string) string {
	memQueryString := `sum by (node) (%s{node!=""} * on(pod,
        namespace) group_left(phase) (kube_pod_status_phase{phase!="Succeeded",phase!="Failed"} == 1))`
	return fmt.Sprintf(memQueryString, resourceName)
}

func getPodMemQuery(namespace string, name string) string {
	memQueryString := `sum by (pod_name) (container_memory_usage_bytes_without_cache{namespace=~"%s",pod_name=~"%s",container_name!="POD"})`
	return fmt.Sprintf(memQueryString, namespace, name)
}

func getPodNvidiaGpuUtilization(namespace string, name string) string {
	memQueryString := `sum by (pod_name) (dcgm_gpu_utilization{pod_namespace=~"%s",pod_name=~"%s",container_name!="POD"})`
	return fmt.Sprintf(memQueryString, namespace, name)
}

func (c *MorgansClient) QueryPodCPU(clusterName string, namespace string, name string) (int64, error) {
	queryString := getCpuQuery(namespace, name)
	vectors, err := c.Query(clusterName, queryString)
	if vectors.Len() != 1 {
		return -1, fmt.Errorf("incorrect len: %d\n", vectors.Len())
	}
	return int64(vectors[0].Value * 1000), juju.Trace(err)
}

func getCpuQuery(namespace string, name string) string {
	memQueryString := `sum by (pod_name) (container_cpu_usage_seconds_total_irate5m{namespace=~"%s",pod_name=~"%s",image!="",container_name!="POD"})`
	return fmt.Sprintf(memQueryString, namespace, name)
}

func (c *MorgansClient) Query(clusterName string, queryString string) (model.Vector, error) {
	request, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s/v1/metrics/%s/prometheus/query", options.MlOptions.MorgansHost, clusterName), nil)
	if err != nil {
		return nil, juju.Trace(err)
	}

	//request.Host = options.MlOptions.MorgansHost

	authHeader := c.req.HeaderParameter(client2.AuthorizationHeader)
	if authHeader == "" || !strings.HasPrefix(authHeader, client2.BearerPrefix) || strings.TrimPrefix(authHeader, client2.BearerPrefix) == "" {
		return nil, errors.NewUnauthorized("No Authorization Bearer Token provided")
	}

	request.Header.Set(client2.AuthorizationHeader, authHeader)

	params := request.URL.Query()
	params.Add("query", queryString)
	request.URL.RawQuery = params.Encode()
	fmt.Printf("%s \n", request.URL)

	response, err := c.httpClient.Do(request)
	if err != nil {
		return nil, juju.Trace(err)
	}

	defer response.Body.Close()
	return c.getValue(response)
}

func (c *MorgansClient) getValue(response *http.Response) (model.Vector, error) {
	value, err := c.getPbValue(response)
	if err != nil {
		return nil, juju.Trace(err)
	}

	var vectors model.Vector
	switch t := value.Type(); t {
	case model.ValVector:
		vectors = value.(model.Vector)
	default:
		return nil, fmt.Errorf("incorrect type %v\n", t)
	}
	return vectors, nil
}

func (c *MorgansClient) getPbValue(response *http.Response) (model.Value, error) {
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, juju.Trace(err)
	}
	var res apiResponse
	err = json.Unmarshal(body, &res)
	if err != nil {
		return nil, juju.Trace(err)
	}
	return model.Value(res.Data.v), err
}

func New(req *restful.Request) (*MorgansClient, error) {
	//tr := &http.Transport{
	//	TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	//}
	client := &http.Client{
		//Transport: tr,
		//Timeout:   time.Duration(1 * time.Second),
	}
	return &MorgansClient{client, req}, nil
}
