package jenkinsbinding

import (
	"alauda.io/amlopr/pkg/apis/base"
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"github.com/emicklei/go-restful"
	"github.com/juju/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"sync"
)

type Processor struct {
	req *restful.Request
	mgr client.Manager
}

// NewProcessor constructor for internal Processor
func NewProcessor(mgr client.Manager, req *restful.Request) Processor {
	return Processor{req, mgr}
}

var Schema = &schema.GroupVersionKind{
	Group:   "devops.alauda.io",
	Kind:    "JenkinsBinding",
	Version: "v1alpha1",
}

func (p Processor) GetJenkinsBinding(namespace string,
	jenkinsBindingName string) (*unstructured.Unstructured, error) {
	dyClient, err := getJenkinsBindingClient(p.mgr, p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	namespacedClient := dyClient.Namespace(namespace)
	return namespacedClient.Get(jenkinsBindingName, metav1.GetOptions{})
}

func (p Processor) ListJenkinsBinding(namespace string,
	options metav1.ListOptions) (*unstructured.UnstructuredList, error) {
	dyClient, err := getJenkinsBindingClient(p.mgr, p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	namespacedClient := dyClient.Namespace(namespace)
	return namespacedClient.List(options)
}

var mutex sync.Mutex

func (p Processor) SetJenkinsCluster(namespace string,
	jenkinsName string, clusterName string) (*unstructured.Unstructured, error) {
	dyClient, err := getJenkinsBindingClient(p.mgr, p)
	if err != nil {
		return nil, errors.Trace(err)
	}
	mutex.Lock()
	defer func() {
		mutex.Unlock()
	}()
	namespacedClient := dyClient.Namespace(namespace)
	err = p.removeOldLabel(clusterName, namespacedClient, namespace)
	if err != nil {
		return nil, errors.Trace(err)
	}

	jenkinsBinding, err := namespacedClient.Get(jenkinsName, metav1.GetOptions{})
	labels := jenkinsBinding.GetLabels()
	for k, v := range base.GetMLJenkinsBindingLabel(clusterName).Get() {
		labels[k] = v
	}
	jenkinsBinding.SetLabels(labels)
	return namespacedClient.Update(jenkinsBinding, metav1.UpdateOptions{})

}

func getJenkinsBindingClient(mgr client.Manager, p Processor) (dynamic.NamespaceableResourceInterface, error) {
	return mgr.DynamicClient(p.req, Schema)
}

func (p Processor) removeOldLabel(clusterName string, namespacedClient dynamic.ResourceInterface, namespace string) error {
	labelSelector := base.GetMLJenkinsBindingLabel(clusterName).GetSelectorString()
	unstructuredList, err := namespacedClient.List(metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		return errors.Trace(err)
	}
	for _, value := range unstructuredList.Items {
		labels := value.GetLabels()
		delete(labels, base.ClusterLabel)
		delete(labels, base.MLLabelAppKey)
		value.SetLabels(labels)
		_, err = namespacedClient.Update(&value, metav1.UpdateOptions{})
		if err != nil {
			return errors.Trace(err)
		}
	}
	return nil
}
