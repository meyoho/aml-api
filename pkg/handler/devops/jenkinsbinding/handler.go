package jenkinsbinding

import (
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
}

func NewHandler(svr server.Server) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
	}
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/jenkinsbindings")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "configmaps")))
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}/{jenkinsCluster}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update jenkinsbinding label").
				Param(ws.PathParameter("namespace", "namespace of the jenkinsbinding")).
				Param(ws.PathParameter("name", "name of jenkinsbinding ")).
				Param(ws.PathParameter("jenkinsCluster", "cluster of jenkinsbinding ")).
				To(handler.UpdateJenkinsCluster).
				Returns(http.StatusOK, "ok", unstructured.Unstructured{}),
		),
	)

	ws.Filter(clientFilter.AMLClientFilter)
	return
}

func (h Handler) UpdateJenkinsCluster(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	jenkinscluster := req.PathParameter("jenkinsCluster")
	var jenkinsBinding *unstructured.Unstructured
	//var requestObj = &v1alpha1.JenkinsBinding{}
	var err error
	defer func() {
		h.WriteResponse(jenkinsBinding, err, req, resp)
	}()
	//if err = req.ReadEntity(requestObj); err != nil {
	//	return
	//}

	jenkinsBinding, err = h.getProcessor(req).SetJenkinsCluster(namespace, name, jenkinscluster)
}

func (h Handler) getProcessor(req *restful.Request) Processor {
	return NewProcessor(h.Server.GetManager(), req)
}
