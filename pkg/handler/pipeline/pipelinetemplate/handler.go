package pipelinetemplate

import (
	"alauda.io/aml-api/pkg/base"
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	RetrievePipelineTemplate(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.AMLPipelineTemplate, error)
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "amlpipelinetemplates")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/amlclusterpipelinetemplates/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a cluster pipelinetemplate").
				Param(ws.PathParameter("name", "name of the pipelinetemplate")).
				To(handler.RetrieveClusterPipelineTemplate).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipelineTemplate{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/amlpipelinetemplates/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a pipelinetemplate").
				Param(ws.PathParameter("namespace", "namespace of the pipelinetemplate")).
				Param(ws.PathParameter("name", "name of the pipelinetemplate")).
				To(handler.RetrievePipelineTemplate).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipelineTemplate{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}
func (h Handler) RetrievePipelineTemplate(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	template, err := h.Processor.RetrievePipelineTemplate(req.Request.Context(), client, namespace, name)
	h.WriteResponse(template, err, req, resp)
	return
}
func (h Handler) RetrieveClusterPipelineTemplate(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	//namespace := req.PathParameter("namespace")
	namespace := base.GetAlaudaNamespace()
	name := req.PathParameter("name")
	template, err := h.Processor.RetrievePipelineTemplate(req.Request.Context(), client, namespace, name)
	h.WriteResponse(template, err, req, resp)
	return
}
