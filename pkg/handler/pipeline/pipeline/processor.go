package pipeline

import (
	"alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//var _ Processor = processor{}

func (p processor) ListPipeline(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *PipelineList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list Pipeline end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   common.GetLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}
	list, err := client.MlV1alpha1().AMLPipelines(namespace.ToRequestParam()).List(listOptions)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data = &PipelineList{
		Items:    make([]v1alpha1.AMLPipeline, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToPipelineSlice(result)
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

func ConvertToPipelineSlice(filtered []metav1.Object) (items []v1alpha1.AMLPipeline) {
	items = make([]v1alpha1.AMLPipeline, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.AMLPipeline); ok {
			items = append(items, *cm)
		}
	}
	return
}

//
func (p processor) RetrievePipeline(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.AMLPipeline, error) {
	ppCfg, err := client.MlV1alpha1().AMLPipelines(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.AMLPipeline{}, err
	}
	return ppCfg, nil
}

func (p processor) CreatePipeline(ctx context.Context, client versioned.Interface, namespace string, requestObj *v1alpha1.AMLPipeline) (*v1alpha1.AMLPipeline, error) {
	return client.MlV1alpha1().AMLPipelines(namespace).Create(requestObj)
}

func (p processor) UpdatePipeline(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.AMLPipeline) (*v1alpha1.AMLPipeline, error) {
	fromObj, err := client.MlV1alpha1().AMLPipelines(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.AMLPipeline{}, err
	}
	to := fromObj.DeepCopy()
	to.Spec.Parameters = requestObj.Spec.Parameters
	return client.MlV1alpha1().AMLPipelines(namespace).Update(to)
}

func (p processor) DeletePipeline(ctx context.Context, client versioned.Interface, namespace, name string) error {
	return client.MlV1alpha1().AMLPipelines(namespace).Delete(name, &metav1.DeleteOptions{})
}

func (p processor) CancelPipeline(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.AMLPipeline) (*v1alpha1.AMLPipeline, error) {
	fromObj, err := client.MlV1alpha1().AMLPipelines(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.AMLPipeline{}, err
	}
	to := fromObj.DeepCopy()
	to.Status.State = v1alpha1.AMLPipelineStateEnum.Cancelled
	to.Status.Phase = v1alpha1.AMLPipelinePhaseEnum.Cancelled
	return client.MlV1alpha1().AMLPipelines(namespace).Update(to)
}
