package pipeline

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	ListPipeline(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*PipelineList, error)
	RetrievePipeline(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.AMLPipeline, error)
	CreatePipeline(ctx context.Context, client versioned.Interface, namespace string, requestObj *v1alpha1.AMLPipeline) (*v1alpha1.AMLPipeline, error)
	UpdatePipeline(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.AMLPipeline) (*v1alpha1.AMLPipeline, error)
	DeletePipeline(ctx context.Context, client versioned.Interface, namespace, name string) error
	CancelPipeline(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.AMLPipeline) (*v1alpha1.AMLPipeline, error)
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/amlpipelines")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "amlpipelines")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List amlpipeline instances`).
					Param(ws.PathParameter("namespace", "namespace of the amlpipeline")).
					To(handler.ListPipeline).
					Writes(v1alpha1.AMLPipeline{}).
					Returns(http.StatusOK, "OK", PipelineList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a amlpipeline").
				Param(ws.PathParameter("namespace", "namespace of the amlpipeline")).
				Param(ws.PathParameter("name", "name of the amlpipeline")).
				To(handler.RetrievePipeline).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipeline{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("create a amlpipeline").
				Param(ws.PathParameter("namespace", "namespace of the amlpipeline")).
				To(handler.CreatePipeline).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipeline{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update a amlpipeline").
				Param(ws.PathParameter("namespace", "namespace of the amlpipeline")).
				Param(ws.PathParameter("name", "name of the amlpipeline")).
				To(handler.UpdatePipeline).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipeline{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}/cancel").
				Filter(clientFilter.AMLClientFilter).
				Doc("cancel a amlpipeline").
				Param(ws.PathParameter("namespace", "namespace of the amlpipeline")).
				Param(ws.PathParameter("name", "name of the amlpipeline")).
				To(handler.CancelPipeline).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipeline{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a amlpipeline").
				Param(ws.PathParameter("namespace", "namespace of the amlpipeline")).
				Param(ws.PathParameter("name", "name of the amlpipeline")).
				To(handler.DeletePipeline).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}

// ListNotebook list Notebook instances
func (h Handler) ListPipeline(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	query := abctx.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	list, err := h.Processor.ListPipeline(req.Request.Context(), client, query, namespace)
	h.WriteResponse(list, err, req, resp)
}

func (h Handler) RetrievePipeline(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.Processor.RetrievePipeline(req.Request.Context(), client, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) CreatePipeline(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	var ppCfg *v1alpha1.AMLPipeline
	var requestObj = &v1alpha1.AMLPipeline{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.Processor.CreatePipeline(req.Request.Context(), client, namespace, requestObj)
}
func (h Handler) UpdatePipeline(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var ppCfg *v1alpha1.AMLPipeline
	var requestObj = &v1alpha1.AMLPipeline{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.Processor.UpdatePipeline(req.Request.Context(), client, namespace, name, requestObj)
}
func (h Handler) DeletePipeline(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	err = h.Processor.DeletePipeline(req.Request.Context(), client, namespace, name)
}

func (h Handler) CancelPipeline(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var ppCfg *v1alpha1.AMLPipeline
	var requestObj = &v1alpha1.AMLPipeline{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.Processor.CancelPipeline(req.Request.Context(), client, namespace, name, requestObj)
}
