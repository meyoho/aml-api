package pipelineconfig

import (
	//"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/aml-api/pkg/api"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
)

// PipelineList contains a list of jenkins in the cluster.
type PipelineConfigList struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of Pipeline.
	Items []Detail `json:"amlpipelineconfigs"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type Detail struct {
	AMLPipelineConfig v1alpha1.AMLPipelineConfig `json:"AMLPipelineConfig"`
	AMLPipelines      []v1alpha1.AMLPipeline     `json:"AMLPipelines"`
	ListMeta          api.ListMeta               `json:"listMeta"`
}
