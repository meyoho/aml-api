package pipelineconfig

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	ListPipelineConfig(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*PipelineConfigList, error)
	RetrievePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.AMLPipelineConfig, error)
	CreatePipelineConfig(ctx context.Context, client versioned.Interface, namespace string, requestObj *v1alpha1.AMLPipelineConfig) (*v1alpha1.AMLPipelineConfig, error)
	UpdatePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.AMLPipelineConfig) (*v1alpha1.AMLPipelineConfig, error)
	DeletePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) error
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/amlpipelineconfigs")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "amlpipelineconfigs")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List amlpipelineconfig instances`).
					Param(ws.PathParameter("namespace", "namespace of the amlpipelineconfig")).
					To(handler.ListPipelineConfig).
					Writes(v1alpha1.AMLPipelineConfig{}).
					Returns(http.StatusOK, "OK", PipelineConfigList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a amlpipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the amlpipelineconfig")).
				Param(ws.PathParameter("name", "name of the amlpipelineconfig")).
				To(handler.RetrievePipelineConfig).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipelineConfig{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("create a amlpipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the amlpipelineconfig")).
				To(handler.CreatePipelineConfig).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipelineConfig{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update a amlpipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the amlpipelineconfig")).
				Param(ws.PathParameter("name", "name of the amlpipelineconfig")).
				To(handler.UpdatePipelineConfig).
				Returns(http.StatusOK, "ok", v1alpha1.AMLPipelineConfig{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a amlpipelineconfig").
				Param(ws.PathParameter("namespace", "namespace of the amlpipelineconfig")).
				Param(ws.PathParameter("name", "name of the amlpipelineconfig")).
				To(handler.DeletePipelineConfig).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}

// ListNotebook list Notebook instances
func (h Handler) ListPipelineConfig(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	query := abctx.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	list, err := h.Processor.ListPipelineConfig(req.Request.Context(), client, query, namespace)
	h.WriteResponse(list, err, req, resp)

}
func (h Handler) RetrievePipelineConfig(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.Processor.RetrievePipelineConfig(req.Request.Context(), client, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) CreatePipelineConfig(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	var ppCfg *v1alpha1.AMLPipelineConfig
	var requestObj = &v1alpha1.AMLPipelineConfig{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.Processor.CreatePipelineConfig(req.Request.Context(), client, namespace, requestObj)
}
func (h Handler) UpdatePipelineConfig(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var ppCfg *v1alpha1.AMLPipelineConfig
	var requestObj = &v1alpha1.AMLPipelineConfig{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.Processor.UpdatePipelineConfig(req.Request.Context(), client, namespace, name, requestObj)
}
func (h Handler) DeletePipelineConfig(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	err = h.Processor.DeletePipelineConfig(req.Request.Context(), client, namespace, name)
}
