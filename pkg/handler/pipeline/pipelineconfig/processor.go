package pipelineconfig

import (
	"alauda.io/aml-api/pkg/api"
	"alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"sort"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//var _ Processor = processor{}

func (p processor) ListPipelineConfig(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *PipelineConfigList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list PipelineConfig end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   common.GetLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}
	list, err := client.MlV1alpha1().AMLPipelineConfigs(namespace.ToRequestParam()).List(listOptions)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}

	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data = ConvertToPipelineConfigSlice(client, result, filteredTotal)
	data.Errors = append(data.Errors, nonCriticalErrors...)
	return
}

func ConvertToPipelineConfigSlice(mlClient versioned.Interface, filtered []metav1.Object, filteredTotal int) (result *PipelineConfigList) {
	maxLen := 5
	result = &PipelineConfigList{ListMeta: common.ListMeta{TotalItems: filteredTotal}}
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.AMLPipelineConfig); ok {
			detail := &Detail{AMLPipelineConfig: *cm}
			amlPipelineList, err := getOwneredAMLPipeline(mlClient, cm)
			if err != nil {
				result.Errors = append(result.Errors, err)
			} else {
				err := addPipeline2Detail(amlPipelineList, detail, maxLen)
				if err != nil {
					result.Errors = append(result.Errors, err)
				}
				result.Items = append(result.Items, *detail)
			}
		}
	}
	return
}

func getOwneredAMLPipeline(mlClient versioned.Interface, amlPipelineConfig *v1alpha1.AMLPipelineConfig) (*v1alpha1.AMLPipelineList, error) {
	listOptions := metav1.ListOptions{
		LabelSelector:   getAMLPipelineConfigSelector(amlPipelineConfig.Name).String(),
		ResourceVersion: "0",
	}
	amlPipelineList, err := mlClient.MlV1alpha1().AMLPipelines(amlPipelineConfig.Namespace).List(listOptions)
	return amlPipelineList, err
}

func addPipeline2Detail(pipelineList *v1alpha1.AMLPipelineList, detail *Detail, maxLen int) error {
	pipelines := pipelineList.Items
	detail.ListMeta = api.ListMeta{TotalItems: len(pipelines)}
	if len(pipelines) > 0 {
		sort.SliceStable(pipelines, func(i, j int) bool {
			return pipelines[i].GetObjectMeta().GetCreationTimestamp().After(pipelines[j].GetObjectMeta().GetCreationTimestamp().Time)
		})

		for _, p := range pipelines {
			if len(detail.AMLPipelines) >= maxLen {
				break
			}
			detail.AMLPipelines = append(detail.AMLPipelines, p)
		}
	}
	return nil
}

func getAMLPipelineConfigSelector(name string) labels.Selector {
	selector := labels.NewSelector()
	req, _ := labels.NewRequirement("AMLPipelineConfig", selection.DoubleEquals, []string{name})
	selector = selector.Add(*req)
	return selector
}

//
func (p processor) RetrievePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.AMLPipelineConfig, error) {
	ppCfg, err := client.MlV1alpha1().AMLPipelineConfigs(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.AMLPipelineConfig{}, err
	}
	return ppCfg, nil
}

func (p processor) CreatePipelineConfig(ctx context.Context, client versioned.Interface, namespace string, requestObj *v1alpha1.AMLPipelineConfig) (*v1alpha1.AMLPipelineConfig, error) {
	return client.MlV1alpha1().AMLPipelineConfigs(namespace).Create(requestObj)
}

func (p processor) UpdatePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.AMLPipelineConfig) (*v1alpha1.AMLPipelineConfig, error) {
	fromObj, err := client.MlV1alpha1().AMLPipelineConfigs(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.AMLPipelineConfig{}, err
	}
	to := fromObj.DeepCopy()
	to.Spec = requestObj.Spec
	to.Status.Phase = v1alpha1.AMLPipelineConfigPhaseEnum.Updating
	return client.MlV1alpha1().AMLPipelineConfigs(namespace).Update(to)
}

func (p processor) DeletePipelineConfig(ctx context.Context, client versioned.Interface, namespace, name string) error {
	return client.MlV1alpha1().AMLPipelineConfigs(namespace).Delete(name, &metav1.DeleteOptions{})
}
