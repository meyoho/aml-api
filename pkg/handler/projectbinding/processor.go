package projectbinding

import (
	"alauda.io/aml-api/pkg/api"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	"encoding/json"
	"fmt"
	"github.com/emicklei/go-restful"
	"github.com/spf13/viper"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/kubernetes"
	"strings"
)

type Processor struct{}

func (Processor) GetMLBindingList(client versioned.Interface, request *restful.Request) (*v1alpha1.MLBinding, error) {
	namespace := request.PathParameter("namespace")
	mlBindingObject, err := client.MlV1alpha1().MLBindings(namespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	if len(mlBindingObject.Items) < 1 {
		return nil, errors.NewInternalError(fmt.Errorf("no aml project in the namespace: %s", namespace))
	}
	i := mlBindingObject.Items[0]
	bytes, err := json.Marshal(i)
	binding := &v1alpha1.MLBinding{}
	err = json.Unmarshal(bytes, binding)
	return binding, err
}

func (Processor) GetMLBinding(client versioned.Interface, request *restful.Request) (*v1alpha1.MLBinding, error) {
	namespace := request.PathParameter("namespace")
	name := request.PathParameter("name")
	return client.MlV1alpha1().MLBindings(namespace).Get(name, metav1.GetOptions{})
}

func (Processor) CreateMLBinding(client versioned.Interface, namespace string, requestObj *v1alpha1.MLBinding) (*v1alpha1.MLBinding, error) {
	return client.MlV1alpha1().MLBindings(namespace).Create(requestObj)
}

func (Processor) UpdateMLBinding(client versioned.Interface, namespace, name string, requestObj *v1alpha1.MLBinding) (*v1alpha1.MLBinding, error) {
	fromObj, err := client.MlV1alpha1().MLBindings(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.MLBinding{}, err
	}
	to := fromObj.DeepCopy()
	to.Spec = requestObj.Spec
	return client.MlV1alpha1().MLBindings(namespace).Update(to)
}

func (Processor) ParseStorage(nb *v1alpha1.NoteBook, mlBinding *v1alpha1.MLBinding) error {
	pvcSlice := mlBinding.Spec.Pvc
	var volumes []v1alpha1.NotebookPVC
	for _, pvc := range pvcSlice {
		pvcMountPath, err := getPvcMountPath(mlBinding)
		if err != nil {
			return nil
		}
		volumes = append(volumes,
			v1alpha1.NotebookPVC{
				ClaimName: pvc.Volume.PersistentVolumeClaim.ClaimName,
				MountPath: joinPvcMountProjectPath(pvcMountPath, pvc.Volume.PersistentVolumeClaim.ClaimName),
				SubPath:   common.SharedProjectFolderName,
				ReadOnly:  false,
			},
			v1alpha1.NotebookPVC{
				ClaimName: pvc.Volume.PersistentVolumeClaim.ClaimName,
				MountPath: joinPvcMountPrivatePath(pvcMountPath, pvc.Volume.PersistentVolumeClaim.ClaimName, nb.Spec.User),
				SubPath:   nb.Spec.User,
				ReadOnly:  false,
			},
		)
	}
	nb.Spec.Pvc = volumes
	return nil
}

func getPvcMountPath(mlBinding *v1alpha1.MLBinding) (string, error) {
	if len(mlBinding.Spec.Pvc) < 1 {
		return "", errors.NewInternalError(fmt.Errorf("you should config a pvc first"))
	}
	return formatPath(mlBinding.Spec.Pvc[0].MountPath), nil

}

func joinPvcMountProjectPath(pvcMountPath string, pvcName string) string {
	return formatPath(pvcMountPath) + "/" + pvcName + "/" + common.SharedProjectFolderName
}

func joinPvcMountPrivatePath(pvcMountPath string, pvcName string, userName string) string {
	return formatPath(pvcMountPath) + "/" + pvcName + "/" + userName
}

func formatPath(path string) string {
	return strings.TrimRight(path, "/")
}

func getMountPath(path string, binding *v1alpha1.MLBinding) (string, error) {
	split := strings.Split(path, "/")
	if len(split) < 2 {
		return "", errors.NewBadRequest(fmt.Sprintf("invalid path: %s", path))
	}
	pvcSlice := binding.Spec.Pvc
	for _, pvc := range pvcSlice {
		if pvc.Volume.Name == split[1] {
			return pvc.MountPath + path, nil
		}
	}
	return "", errors.NewBadRequest(fmt.Sprintf("cannot find the mount path for url: %s", path))
}

func getPvcSize(k8sClient kubernetes.Interface, namespace, pvcName string) (int64, error) {
	pvcDetail, err, _ := getPersistentVolumeClaimDetail(k8sClient, namespace, pvcName)
	if err != nil {
		return -1, err
	}
	c, ok := pvcDetail.Capacity[corev1.ResourceStorage]
	if !ok {
		return 0, nil
	}
	return (&c).Value(), nil
}
func getPersistentVolumeClaimDetail(client kubernetes.Interface, namespace string, name string) (*PersistentVolumeClaimDetail, error, *corev1.PersistentVolumeClaim) {
	getOpts := metav1.GetOptions{
		ResourceVersion: "0",
	}
	rawPersistentVolumeClaim, err := client.CoreV1().PersistentVolumeClaims(namespace).Get(name, getOpts)
	if err != nil {
		return nil, err, nil
	}
	setTypeMeta(rawPersistentVolumeClaim)
	details, err := getPersistentVolumeClaimDetailAndAppName(rawPersistentVolumeClaim)
	if err != nil {
		return nil, err, nil
	}
	return details, nil, rawPersistentVolumeClaim
}
func setTypeMeta(cm *corev1.PersistentVolumeClaim) {
	cm.TypeMeta.SetGroupVersionKind(schema.GroupVersionKind{
		Group:   "",
		Version: "v1",
		Kind:    "PersistentVolumeClaim",
	})
}
func getPersistentVolumeClaimDetailAndAppName(rawPersistentVolumeClaim *corev1.PersistentVolumeClaim) (*PersistentVolumeClaimDetail, error) {

	rawPersistentVolumeClaim.Kind = api.ResourceKindPersistentVolumeClaim
	rawPersistentVolumeClaim.APIVersion = "v1"
	uns, err := ConvertResourceToUnstructured(rawPersistentVolumeClaim)
	if err != nil {
		return nil, err
	}

	details := &PersistentVolumeClaimDetail{
		ObjectMeta:   api.NewObjectMeta(rawPersistentVolumeClaim.ObjectMeta),
		TypeMeta:     api.NewTypeMeta(api.ResourceKindPersistentVolumeClaim),
		AppName:      findApplicationName(GetLocalBaseDomain(), uns),
		Status:       rawPersistentVolumeClaim.Status.Phase,
		Volume:       rawPersistentVolumeClaim.Spec.VolumeName,
		Capacity:     rawPersistentVolumeClaim.Status.Capacity,
		AccessModes:  rawPersistentVolumeClaim.Spec.AccessModes,
		StorageClass: rawPersistentVolumeClaim.Spec.StorageClassName,
	}

	return details, nil
}

func findApplicationName(baseDomain string, resource *unstructured.Unstructured) string {
	if resource.GetLabels() == nil {
		return ""
	}
	v := resource.GetLabels()[getAppNameKey(baseDomain)]
	names := strings.Split(v, ".")
	if len(names) != 2 {
		return ""
	}
	return names[0]
}
func GetLocalBaseDomain() string {
	if viper.GetString("LABEL_BASE_DOMAIN") != "" {
		return viper.GetString("LABEL_BASE_DOMAIN")
	}
	return "alauda.io"
}
func ConvertResourceToUnstructured(resource interface{}) (*unstructured.Unstructured, error) {
	unstr := &unstructured.Unstructured{}
	data, err := json.Marshal(resource)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, unstr)
	if err != nil {
		return nil, err
	}
	return unstr, nil
}

func getAppNameKey(baseDomain string) string {
	if baseDomain == "" {
		baseDomain = DefaultBaseDomain
	}
	return fmt.Sprintf("app.%s/name", baseDomain)
}

type PersistentVolumeClaimDetail struct {
	ObjectMeta   api.ObjectMeta                      `json:"objectMeta"`
	TypeMeta     api.TypeMeta                        `json:"typeMeta"`
	AppName      string                              `json:"appName"`
	Status       corev1.PersistentVolumeClaimPhase   `json:"status"`
	Volume       string                              `json:"volume"`
	Capacity     corev1.ResourceList                 `json:"capacity"`
	AccessModes  []corev1.PersistentVolumeAccessMode `json:"accessModes"`
	StorageClass *string                             `json:"storageClass"`
}

const (
	DefaultBaseDomain = "alauda.io"
)
