package projectbinding

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	apierrors "alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"fmt"
	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, Processor{})
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/amlprojectbindings")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "mlbinding")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get mlbinding").
				Param(ws.PathParameter("namespace", "namespace of the mlbinding")).
				Param(ws.PathParameter("name", "name of the mlbinding")).
				To(handler.GetProjectBinding).
				Returns(http.StatusOK, "ok", v1alpha1.MLBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{cluster}/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("create mlbinding").
				Param(ws.PathParameter("namespace", "namespace of the mlbinding")).
				To(handler.CreateProjectBinding).
				Returns(http.StatusOK, "ok", v1alpha1.MLBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update mlbinding").
				Param(ws.PathParameter("namespace", "namespace of the mlbinding")).
				Param(ws.PathParameter("name", "name of the mlbinding")).
				To(handler.UpdateProjectBinding).
				Returns(http.StatusOK, "ok", v1alpha1.MLBinding{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/pvcstat/mountpath/{cluster}/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get pvc-mount-path").
				Param(ws.PathParameter("cluster", "cluster of the pvc-mount-path")).
				Param(ws.PathParameter("namespace", "namespace of pvc-mount-path")).
				To(handler.RetrievePvcMountPath).
				Returns(http.StatusOK, "ok", NotebookPodPath{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/pvcstat/size/{cluster}/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Filter(clientFilter.SecureFilter).
				Doc("get pvc-size").
				Param(ws.PathParameter("cluster", "cluster of the pvc-size")).
				Param(ws.PathParameter("namespace", "namespace of the pvc-size")).
				To(handler.RetrievePvcSize).
				Returns(http.StatusOK, "ok", PvcSize{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/pvcstat/realpath/{cluster}/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get mount-path").
				Param(ws.PathParameter("cluster", "cluster of the mount-path")).
				Param(ws.PathParameter("namespace", "namespace of the mount-path")).
				To(handler.RetrieveMountPath).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}

func (h Handler) UpdateProjectBinding(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var ppCfg *v1alpha1.MLBinding
	var requestObj = &v1alpha1.MLBinding{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.Processor.UpdateMLBinding(client, namespace, name, requestObj)
}

func (h Handler) GetProjectBinding(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	projectBinding, err := h.Processor.GetMLBinding(client, req)
	h.WriteResponse(projectBinding, err, req, resp)
	return
}

func (h Handler) CreateProjectBinding(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	var requestObj = &v1alpha1.MLBinding{}
	var err error
	if err = req.ReadEntity(requestObj); err != nil {
		h.WriteResponse(requestObj, err, req, resp)
		return
	}
	projectBinding, err := h.Processor.CreateMLBinding(client, namespace, requestObj)
	h.WriteResponse(projectBinding, err, req, resp)
	return
}

func (h Handler) RetrievePvcMountPath(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	userName := req.HeaderParameter("userName")
	var err error
	var path *NotebookPodPath
	defer func() {
		h.WriteResponse(path, err, req, resp)
	}()
	mlBinding, err := h.Processor.GetMLBindingList(client, req)
	if err != nil {
		return
	}
	mountPath, err := getPvcMountPath(mlBinding)
	if err != nil {
		return
	}
	path = &NotebookPodPath{
		ProjectPath: joinPvcMountProjectPath(mountPath, "{pvc}"),
		PrivatePath: joinPvcMountPrivatePath(mountPath, "{pvc}", userName),
	}
}
func (h Handler) RetrievePvcSize(req *restful.Request, resp *restful.Response) {
	k8sclient := abctx.Client(req.Request.Context())
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	var err error
	var size = &PvcSize{}
	defer func() {
		h.WriteResponse(size, err, req, resp)
	}()
	mlBinding, err := h.Processor.GetMLBindingList(client, req)
	if err != nil {
		return
	}
	if len(mlBinding.Spec.Pvc) < 1 {
		err = errors.NewConflict(
			schema.GroupResource{
				Group:    mlBinding.GetObjectKind().GroupVersionKind().Group,
				Resource: mlBinding.Kind,
			}, mlBinding.Name,
			fmt.Errorf("you should config a pvc first"))
		return
	}
	var result int64 = 0
	for _, pvc := range mlBinding.Spec.Pvc {
		pvcName := pvc.Volume.PersistentVolumeClaim.ClaimName
		pvcSize, err := getPvcSize(k8sclient, namespace, pvcName)
		if err != nil {
			apierrors.HandleInternalError(resp, err)
		}
		result += pvcSize
	}
	size.Size = result
}
func (h Handler) RetrieveMountPath(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	path := req.QueryParameter("path")
	var err error
	var mountPath string
	defer func() {
		h.WriteResponse(mountPath, err, req, resp)
	}()
	mlBinding, err := h.Processor.GetMLBindingList(client, req)
	if err != nil {
		return
	}
	mountPath, err = getMountPath(path, mlBinding)
}
