package pytorchjob

import (
	localctx "alauda.io/aml-api/pkg/context"
	podutil "alauda.io/aml-api/pkg/handler/pod"
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"bitbucket.org/mathildetech/log"
	"fmt"
	"github.com/emicklei/go-restful"
	pyTorchjob "github.com/kubeflow/pytorch-operator/pkg/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type JobProcessor struct {
	req *restful.Request
	mgr client.Manager
}

// NewProcessor constructor for internal processor
func NewJobProcessor(mgr client.Manager, req *restful.Request) JobProcessor {
	return JobProcessor{req, mgr}
}

func (p JobProcessor) RetrievePyTorchJob(namespace, name string) (*PyTorchJobDetail, error) {
	job, err := p.getClient().KubeflowV1beta2().PyTorchJobs(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &PyTorchJobDetail{}, err
	}
	pyTorchJobDetail := PyTorchJobDetail{
		PyTorchJob: job,
	}

	log.Infof("successfully listed pods for TFJob %v under namespace %v", name, namespace)

	podProcessor := podutil.NewProcessor(p.mgr, p.req)

	pods, err := podProcessor.ListPods(namespace, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("group-name=kubeflow.org,pytorch-job-name=%s", name),
	})

	if err != nil {
		return &PyTorchJobDetail{}, err
	} else {
		log.Infof("successfully listed pods for TFJob %v under namespace %v", name, namespace)
		podDetails, err := podProcessor.GetPodDetails(pods)
		if err != nil {
			return &PyTorchJobDetail{}, err
		}
		pyTorchJobDetail.Pods = podDetails
	}

	return &pyTorchJobDetail, nil
}

func (p JobProcessor) getClient() pyTorchjob.Interface {
	return localctx.PytorchClient(p.req.Request.Context())
}

func (p JobProcessor) DeletePyTorchJob(namespace, name string) error {
	return p.getClient().KubeflowV1beta2().PyTorchJobs(namespace).Delete(name, &metav1.DeleteOptions{})
}
