package pytorchjob

import (
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"fmt"
	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/api/errors"
	"net/http"
)

type JobHandler struct {
	*common.AbstractHandler
}

func NewJobHandler(svr server.Server) JobHandler {
	return JobHandler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
	}
}

func NewJob(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewJobHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/pytorchjobs")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pytorchjobs")))

	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a pytorchjobs").
				Param(ws.PathParameter("cluster", "cluster of the pytorchjobs")).
				Param(ws.PathParameter("namespace", "namespace of the pytorchjobs")).
				Param(ws.PathParameter("name", "name of the pytorchjobs")).
				To(handler.RetrievePyTorchJob).
				Returns(http.StatusOK, "ok", v1alpha1.PyTorchTraining{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a pytorchjobs").
				Param(ws.PathParameter("cluster", "cluster of the pytorchjobs")).
				Param(ws.PathParameter("namespace", "namespace of the pytorchjobs")).
				To(handler.DeletePyTorchJob).
				Returns(http.StatusOK, "ok", v1alpha1.PyTorchTraining{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}

// TODO missing metrics info, how to support?
func (h JobHandler) RetrievePyTorchJob(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	pyTorchJobDetail, err := h.getProcessor(req).RetrievePyTorchJob(namespace, name)
	if errors.IsNotFound(err) {
		pyTorchJobDetail, err = &PyTorchJobDetail{Error: fmt.Sprintf("tfjob %s not found", name)}, nil
	}
	h.WriteResponse(pyTorchJobDetail, err, req, resp)
	return
}
func (h JobHandler) DeletePyTorchJob(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	err = h.getProcessor(req).DeletePyTorchJob(namespace, name)
}

func (h JobHandler) getProcessor(req *restful.Request) JobProcessor {
	return NewJobProcessor(h.Server.GetManager(), req)
}
