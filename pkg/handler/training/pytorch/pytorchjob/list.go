package pytorchjob

import (
	"alauda.io/aml-api/pkg/handler/pod"
	pytorchv1beta2 "github.com/kubeflow/pytorch-operator/pkg/apis/pytorch/v1beta2"
)

type PyTorchJobDetail struct {
	PyTorchJob *pytorchv1beta2.PyTorchJob `json:"pytorchJob"`
	Pods       []pod.PodDetail            `json:"pods"`
	Error      string                     `json:"error"`
}
