package pytorch

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	//ListTensorflowTraining(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*TensorflowTrainingList, error)
	RetrievePyTorchTraining(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.PyTorchTraining, error)
	//UpdateTensorflowTraining(ctx context.Context, client versioned.Interface, namespace, name string, notebook *v1alpha1.TensorflowTraining) (*v1alpha1.TensorflowTraining, error)
	DeletePyTorchTraining(ctx context.Context, client versioned.Interface, namespace, name string) error
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/pytorchtrainings")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "pytorchtrainings")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a pytorchtrainings").
				Param(ws.PathParameter("cluster", "cluster of the pytorchtrainings")).
				Param(ws.PathParameter("namespace", "namespace of the pytorchtrainings")).
				Param(ws.PathParameter("name", "name of the pytorchtrainings")).
				To(handler.RetrievePyTorchTraining).
				Returns(http.StatusOK, "ok", v1alpha1.PyTorchTraining{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a pytorchtrainings").
				Param(ws.PathParameter("cluster", "cluster of the pytorchtrainings")).
				Param(ws.PathParameter("namespace", "namespace of the pytorchtrainings")).
				To(handler.DeletePyTorchTraining).
				Returns(http.StatusOK, "ok", v1alpha1.PyTorchTraining{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}
func (h Handler) RetrievePyTorchTraining(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.Processor.RetrievePyTorchTraining(req.Request.Context(), client, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) DeletePyTorchTraining(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	err = h.Processor.DeletePyTorchTraining(req.Request.Context(), client, namespace, name)
}
