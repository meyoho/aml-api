package pytorch

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}
func (p processor) RetrievePyTorchTraining(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.PyTorchTraining, error) {
	ins, err := client.MlV1alpha1().PyTorchTrainings(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.PyTorchTraining{}, err
	}
	return ins, nil
}

func (p processor) DeletePyTorchTraining(ctx context.Context, client versioned.Interface, namespace, name string) error {
	return client.MlV1alpha1().PyTorchTrainings(namespace).Delete(name, &metav1.DeleteOptions{})
}
