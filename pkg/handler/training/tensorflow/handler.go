package tensorflow

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	ListTensorflowTraining(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*TensorflowTrainingList, error)
	RetrieveTensorflowTraining(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.TensorflowTraining, error)
	UpdateTensorflowTraining(ctx context.Context, client versioned.Interface, namespace, name string, tf *v1alpha1.TensorflowTraining) (*v1alpha1.TensorflowTraining, error)
	DeleteTensorflowTraining(ctx context.Context, client versioned.Interface, namespace, name string) error
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/tensorflowtrainings")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "notebooks")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{cluster}/{namespace}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List tensorflowtrainings instance`).
					Param(ws.PathParameter("cluster", "cluster of the tensorflowtrainings")).
					Param(ws.PathParameter("namespace", "namespace of the tensorflowtrainings")).
					To(handler.ListTensorflowTraining).
					Writes(v1alpha1.TensorflowTraining{}).
					Returns(http.StatusOK, "OK", TensorflowTrainingList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a tensorflowtrainings").
				Param(ws.PathParameter("cluster", "cluster of the tensorflowtrainings")).
				Param(ws.PathParameter("namespace", "namespace of the tensorflowtrainings")).
				Param(ws.PathParameter("name", "name of the tensorflowtrainings")).
				To(handler.RetrieveTensorflowTraining).
				Returns(http.StatusOK, "ok", v1alpha1.TensorflowTraining{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update a tensorflowtrainings").
				Param(ws.PathParameter("cluster", "cluster of the tensorflowtrainings")).
				Param(ws.PathParameter("namespace", "namespace of the tensorflowtrainings")).
				To(handler.UpdateTensorflowTraining).
				Returns(http.StatusOK, "ok", v1alpha1.TensorflowTraining{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a tensorflowtrainings").
				Param(ws.PathParameter("cluster", "cluster of the tensorflowtrainings")).
				Param(ws.PathParameter("namespace", "namespace of the tensorflowtrainings")).
				To(handler.DeleteTensorflowTraining).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}

// ListNotebook list Notebook instances
func (h Handler) ListTensorflowTraining(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	query := abctx.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	list, err := h.Processor.ListTensorflowTraining(req.Request.Context(), client, query, namespace)
	h.WriteResponse(list, err, req, resp)

}
func (h Handler) RetrieveTensorflowTraining(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.Processor.RetrieveTensorflowTraining(req.Request.Context(), client, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) UpdateTensorflowTraining(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var tf *v1alpha1.TensorflowTraining
	var requestObj = &v1alpha1.TensorflowTraining{}
	var err error
	defer func() {
		h.WriteResponse(tf, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	tf, err = h.Processor.UpdateTensorflowTraining(req.Request.Context(), client, namespace, name, requestObj)
}
func (h Handler) DeleteTensorflowTraining(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	err = h.Processor.DeleteTensorflowTraining(req.Request.Context(), client, namespace, name)
}
