package tensorflow

import (
	"alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	"fmt"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"strings"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//var _ Processor = processor{}

func (p processor) ListTensorflowTraining(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *TensorflowTrainingList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list TensorflowTraining end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   getLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}
	list, err := client.MlV1alpha1().TensorflowTrainings(namespace.ToRequestParam()).List(listOptions)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data = &TensorflowTrainingList{
		Items:    make([]v1alpha1.TensorflowTraining, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToTensorflowTrainingSlice(result)
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

func ConvertToTensorflowTrainingSlice(filtered []metav1.Object) (items []v1alpha1.TensorflowTraining) {
	items = make([]v1alpha1.TensorflowTraining, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.TensorflowTraining); ok {
			items = append(items, *cm)
		}
	}
	return
}

func getLabelSelectorByDsQuery(dsQuery *dataselect.Query) labels.Selector {
	labelSelector := labels.NewSelector()
	if dsQuery == nil || dsQuery.FilterQuery == nil || len(dsQuery.FilterQuery.FilterByList) == 0 {
		return labelSelector
	}

	for _, filterBy := range dsQuery.FilterQuery.FilterByList {
		if filterBy.Property != "label" {
			continue
		}

		filterStrs := strings.Split(fmt.Sprintf("%s", filterBy.Value), ",")
		for _, filterStr := range filterStrs {
			keyAndValue := strings.Split(filterStr, ":")
			if len(keyAndValue) == 2 {
				req, _ := labels.NewRequirement(keyAndValue[0], selection.DoubleEquals, []string{keyAndValue[1]})
				labelSelector = labelSelector.Add(*req)
			}
		}
	}

	return labelSelector
}

//
func (p processor) RetrieveTensorflowTraining(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.TensorflowTraining, error) {
	ins, err := client.MlV1alpha1().TensorflowTrainings(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.TensorflowTraining{}, err
	}
	return ins, nil
}

func (p processor) UpdateTensorflowTraining(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.TensorflowTraining) (*v1alpha1.TensorflowTraining, error) {
	fromObj, err := client.MlV1alpha1().TensorflowTrainings(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.TensorflowTraining{}, err
	}
	to := fromObj.DeepCopy()
	to.ObjectMeta.Annotations = requestObj.ObjectMeta.Annotations
	if requestObj.ObjectMeta.Labels != nil {
		to.ObjectMeta.Labels = requestObj.ObjectMeta.Labels
	}
	to.Spec = requestObj.Spec
	return client.MlV1alpha1().TensorflowTrainings(namespace).Update(to)
}

func (p processor) DeleteTensorflowTraining(ctx context.Context, client versioned.Interface, namespace, name string) error {
	return client.MlV1alpha1().TensorflowTrainings(namespace).Delete(name, &metav1.DeleteOptions{})
}
