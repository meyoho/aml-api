package tfjob

import (
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"fmt"
	"github.com/emicklei/go-restful"
	tfjobv1beta2 "github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta2"
	"k8s.io/apimachinery/pkg/api/errors"
	"net/http"
)

type JobHandler struct {
	*common.AbstractHandler
}

func NewJobHandler(svr server.Server) JobHandler {
	return JobHandler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
	}
}

func NewJob(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewJobHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/tfjobs")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "tfjobs")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{cluster}/{namespace}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List tfjob instance`).
					Param(ws.PathParameter("cluster", "cluster of the tfjob")).
					Param(ws.PathParameter("namespace", "namespace of the tfjob")).
					To(handler.ListTFJob).
					Writes(tfjobv1beta2.TFJob{}).
					Returns(http.StatusOK, "OK", TFJobList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a tfjobs").
				Param(ws.PathParameter("cluster", "cluster of the tfjobs")).
				Param(ws.PathParameter("namespace", "namespace of the tfjobs")).
				Param(ws.PathParameter("name", "name of the tfjobs")).
				To(handler.RetrieveTFJob).
				Returns(http.StatusOK, "ok", tfjobv1beta2.TFJob{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{cluster}/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("create a tfjobs").
				Param(ws.PathParameter("cluster", "cluster of the tfjobs")).
				Param(ws.PathParameter("namespace", "namespace of the tfjobs")).
				To(handler.CreateTFJob).
				Returns(http.StatusOK, "ok", tfjobv1beta2.TFJob{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a tfjobs").
				Param(ws.PathParameter("cluster", "cluster of the tfjobs")).
				Param(ws.PathParameter("namespace", "namespace of the tfjobs")).
				To(handler.DeleteTFJob).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}

func (h JobHandler) RetrieveTFJob(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	tfJobDetail, err := h.getProcessor(req).RetrieveTFJob(namespace, name)
	if errors.IsNotFound(err) {
		tfJobDetail, err = &TfJobDetail{Error: fmt.Sprintf("tfjob %s not found", name)}, nil
	}
	h.WriteResponse(tfJobDetail, err, req, resp)
	return
}

func (h JobHandler) DeleteTFJob(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	err = h.getProcessor(req).DeleteTFJob(namespace, name)
}

func (h JobHandler) CreateTFJob(req *restful.Request, resp *restful.Response) {
	namespace := req.PathParameter("namespace")
	var tfJob *tfjobv1beta2.TFJob
	var requestObj = &tfjobv1beta2.TFJob{}
	var err error
	defer func() {
		h.WriteResponse(tfJob, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	tfJob, err = h.getProcessor(req).CreateTFJob(namespace, requestObj)
}

func (h JobHandler) ListTFJob(req *restful.Request, resp *restful.Response) {
	query := abctx.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	list, err := h.getProcessor(req).ListTFJob(query, namespace)
	h.WriteResponse(list, err, req, resp)

}

func (h JobHandler) getProcessor(req *restful.Request) JobProcessor {
	return NewJobProcessor(h.Server.GetManager(), req)
}
