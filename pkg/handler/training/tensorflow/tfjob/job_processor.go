package tfjob

import (
	localctx "alauda.io/aml-api/pkg/context"
	mlerrors "alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	podutil "alauda.io/aml-api/pkg/handler/pod"
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"fmt"
	"github.com/emicklei/go-restful"
	"github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta2"
	tfjob "github.com/kubeflow/tf-operator/pkg/client/clientset/versioned"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type JobProcessor struct {
	req *restful.Request
	mgr client.Manager
}

// NewProcessor constructor for internal processor
func NewJobProcessor(mgr client.Manager, req *restful.Request) JobProcessor {
	return JobProcessor{req, mgr}
}

func (p JobProcessor) RetrieveTFJob(namespace, name string) (*TfJobDetail, error) {
	job, err := p.getClient().KubeflowV1beta2().TFJobs(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &TfJobDetail{}, err
	}
	tfJobDetail := TfJobDetail{
		TFJob: job,
	}

	log.Infof("successfully listed pods for TFJob %v under namespace %v", name, namespace)

	podProcessor := podutil.NewProcessor(p.mgr, p.req)

	pods, err := podProcessor.ListPods(namespace, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("group-name=kubeflow.org,tf-job-name=%s", name),
	})

	if err != nil {
		return &TfJobDetail{}, err
	} else {
		log.Infof("successfully listed pods for TFJob %v under namespace %v", name, namespace)
		podDetails, err := podProcessor.GetPodDetails(pods)
		if err != nil {
			return &TfJobDetail{}, err
		}
		tfJobDetail.Pods = podDetails
	}

	return &tfJobDetail, nil
}

func (p JobProcessor) getClient() tfjob.Interface {
	return localctx.TensorflowClient(p.req.Request.Context())
}

func (p JobProcessor) DeleteTFJob(namespace, name string) error {
	return p.getClient().KubeflowV1beta2().TFJobs(namespace).Delete(name, &metav1.DeleteOptions{})
}
func (p JobProcessor) CreateTFJob(namespace string, requestObj *v1beta2.TFJob) (*v1beta2.TFJob, error) {
	return p.getClient().KubeflowV1beta2().TFJobs(namespace).Create(requestObj)
}
func (p JobProcessor) ListTFJob(query *dataselect.Query, namespace *common.NamespaceQuery) (data *TFJobList, err error) {
	logger := abcontext.Logger(p.req.Request.Context())
	defer func() {
		logger.Debug("list TFJob end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   common.GetLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}
	list, err := p.getClient().KubeflowV1beta1().TFJobs(namespace.ToRequestParam()).List(listOptions)
	nonCriticalErrors, criticalError := mlerrors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data = &TFJobList{
		Items:    make([]v1beta2.TFJob, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToTFJobSlice(result)
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return

}

func ConvertToTFJobSlice(filtered []metav1.Object) (items []v1beta2.TFJob) {
	items = make([]v1beta2.TFJob, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1beta2.TFJob); ok {
			items = append(items, *cm)
		}
	}
	return
}
