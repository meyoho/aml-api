package tfjob

import (
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/aml-api/pkg/handler/pod"
	tfv1beta2 "github.com/kubeflow/tf-operator/pkg/apis/tensorflow/v1beta2"
)

type TfJobDetail struct {
	TFJob *tfv1beta2.TFJob `json:"tfJob"`
	Pods  []pod.PodDetail  `json:"pods"`
	Error string           `json:"error"`
}

type TFJobList struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of Pipeline.
	Items []tfv1beta2.TFJob `json:"tfjobs"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}
