package tensorflow

import (
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
)

// PipelineList contains a list of jenkins in the cluster.
type TensorflowTrainingList struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of Pipeline.
	Items []v1alpha1.TensorflowTraining `json:"tensorflow"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}
