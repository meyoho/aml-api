package dataviewer

import (
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//var _ Processor = processor{}
func (p processor) RetrieveDataViewer(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.DataViewer, error) {
	dataViewer, err := client.MlV1alpha1().DataViewers(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.DataViewer{}, err
	}
	return dataViewer, nil
}

func (p processor) UpdateDataViewer(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.DataViewer) (*v1alpha1.DataViewer, error) {
	fromObj, err := client.MlV1alpha1().DataViewers(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.DataViewer{}, err
	}
	to := fromObj.DeepCopy()
	to.Status.Phase = requestObj.Status.Phase
	to.Spec = requestObj.Spec
	return client.MlV1alpha1().DataViewers(namespace).Update(to)
}
