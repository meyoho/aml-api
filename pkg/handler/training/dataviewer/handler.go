package dataviewer

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	RetrieveDataViewer(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.DataViewer, error)
	UpdateDataViewer(ctx context.Context, client versioned.Interface, namespace, name string, requestObj *v1alpha1.DataViewer) (*v1alpha1.DataViewer, error)
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/dataviewers")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "dataviewers")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a dataviewers").
				Param(ws.PathParameter("cluster", "cluster of the dataviewers")).
				Param(ws.PathParameter("namespace", "namespace of the dataviewers")).
				Param(ws.PathParameter("name", "name of the dataviewers")).
				To(handler.RetrieveDataViewer).
				Returns(http.StatusOK, "ok", v1alpha1.DataViewer{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update a dataviewers").
				Param(ws.PathParameter("cluster", "cluster of the dataviewers")).
				Param(ws.PathParameter("namespace", "namespace of the dataviewers")).
				Param(ws.PathParameter("name", "name of the dataviewers")).
				To(handler.UpdateDataViewer).
				Returns(http.StatusOK, "ok", v1alpha1.DataViewer{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}

func (h Handler) RetrieveDataViewer(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.Processor.RetrieveDataViewer(req.Request.Context(), client, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) UpdateDataViewer(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var ppCfg *v1alpha1.DataViewer
	var requestObj = &v1alpha1.DataViewer{}
	var err error
	defer func() {
		h.WriteResponse(ppCfg, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	ppCfg, err = h.Processor.UpdateDataViewer(req.Request.Context(), client, namespace, name, requestObj)
}
