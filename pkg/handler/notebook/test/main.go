package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("123")
	cache := NewCache()
	cache.Put("name", "fy", time.Now().Add(2*time.Second).UnixNano())
	cache.Put("name2", "lisi", time.Now().Add(10*time.Second).UnixNano())
	go func() {
		for now := range time.Tick(1 * time.Second) {
			fmt.Printf("------ %v\n", now)
			vName, err := cache.Get("name")
			fmt.Printf("name value=%s ,err=%v\n", vName, err)
			vName2, err := cache.Get("name2")
			fmt.Printf("name2 value=%s ,err=%v\n", vName2, err)
		}
	}()
	time.Sleep(15 * time.Second)
}

type Item struct {
	value string
	ttl   int64
}

type Cache struct {
	values map[string]Item
}

func (c *Cache) Put(key, value string, ttl int64) {
	c.values[key] = Item{value: value, ttl: ttl}
}
func (c *Cache) Get(key string) (string, error) {
	_, ok := c.values[key]
	if !ok {
		return "", fmt.Errorf("not found")
	}
	return c.values[key].value, nil
}

func NewCache() *Cache {
	m := make(map[string]Item)
	var ticker = time.NewTicker(1 * time.Second)
	go func() {
		for now := range ticker.C {
			for k, v := range m {
				if now.UnixNano() > v.ttl {
					delete(m, k)
				}
			}
		}
	}()
	return &Cache{values: m}
}
