package notebook

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/aml-api/pkg/handler/projectbinding"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor               Processor
	projectBindingProcessor projectbinding.Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler:         &common.AbstractHandler{Server: svr},
		Processor:               processor,
		projectBindingProcessor: projectbinding.Processor{},
	}
}

type Processor interface {
	ListNotebook(ctx context.Context, amlClient versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*NotebookList, error)
	RetrieveNotebook(ctx context.Context, amlClient versioned.Interface, namespace, name string) (*v1alpha1.NoteBook, error)
	CreateNotebook(ctx context.Context, amlClient versioned.Interface, regionClient kubernetes.Interface, namespace string, notebook *v1alpha1.NoteBook) (*v1alpha1.NoteBook, error)
	UpdateNotebook(ctx context.Context, amlClient versioned.Interface, regionClient kubernetes.Interface, namespace, name string, notebook *v1alpha1.NoteBook) (*v1alpha1.NoteBook, error)
	DeleteNotebook(ctx context.Context, amlClient versioned.Interface, regionClient kubernetes.Interface, namespace, name string) error
	StartNotebook(ctx context.Context, amlClient versioned.Interface, namespace, name string) (*v1alpha1.NoteBook, error)
	StopNotebook(ctx context.Context, amlClient versioned.Interface, namespace, name string) (*v1alpha1.NoteBook, error)
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/notebooks")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "notebooks")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{cluster}/{namespace}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List Notebook instance`).
					Param(ws.PathParameter("cluster", "cluster of the notebook")).
					Param(ws.PathParameter("namespace", "namespace of the notebook")).
					To(handler.ListNotebook).
					Writes(v1alpha1.NoteBook{}).
					Returns(http.StatusOK, "OK", NotebookList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a notebook").
				Param(ws.PathParameter("cluster", "cluster of the notebook")).
				Param(ws.PathParameter("namespace", "namespace of the notebook")).
				Param(ws.PathParameter("name", "name of the notebook")).
				To(handler.RetrieveNotebook).
				Returns(http.StatusOK, "ok", v1alpha1.NoteBook{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.POST("/{cluster}/{namespace}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AMLClientFilter).
				Doc("create a notebook").
				Param(ws.PathParameter("cluster", "cluster of the notebook")).
				Param(ws.PathParameter("namespace", "namespace of the notebook")).
				To(handler.CreateNotebook).
				Returns(http.StatusOK, "ok", v1alpha1.NoteBook{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AMLClientFilter).
				Doc("update a notebook").
				Param(ws.PathParameter("cluster", "cluster of the notebook")).
				Param(ws.PathParameter("namespace", "namespace of the notebook")).
				Param(ws.PathParameter("name", "name of the notebook")).
				To(handler.UpdateNotebook).
				Returns(http.StatusOK, "ok", v1alpha1.NoteBook{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{cluster}/{namespace}/{name}/start").
				Filter(clientFilter.AMLClientFilter).
				Doc("start a notebook").
				Param(ws.PathParameter("cluster", "cluster of the notebook")).
				Param(ws.PathParameter("namespace", "namespace of the notebook")).
				Param(ws.PathParameter("name", "name of the notebook")).
				To(handler.StartNotebook).
				Returns(http.StatusOK, "ok", v1alpha1.NoteBook{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{cluster}/{namespace}/{name}/stop").
				Filter(clientFilter.AMLClientFilter).
				Doc("stop a notebook").
				Param(ws.PathParameter("cluster", "cluster of the notebook")).
				Param(ws.PathParameter("namespace", "namespace of the notebook")).
				Param(ws.PathParameter("name", "name of the notebook")).
				To(handler.StopNotebook).
				Returns(http.StatusOK, "ok", v1alpha1.NoteBook{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a notebook").
				Param(ws.PathParameter("cluster", "cluster of the notebook")).
				Param(ws.PathParameter("namespace", "namespace of the notebook")).
				Param(ws.PathParameter("name", "name of the notebook")).
				To(handler.DeleteNotebook).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	return
}

// ListNotebook list Notebook instances
func (h Handler) ListNotebook(req *restful.Request, resp *restful.Response) {
	amlClient := localctx.AMLClient(req.Request.Context())
	query := abctx.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	list, err := h.Processor.ListNotebook(req.Request.Context(), amlClient, query, namespace)
	h.WriteResponse(list, err, req, resp)
}
func (h Handler) RetrieveNotebook(req *restful.Request, resp *restful.Response) {
	amlClient := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.Processor.RetrieveNotebook(req.Request.Context(), amlClient, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) CreateNotebook(req *restful.Request, resp *restful.Response) {
	var err error
	amlClient := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	var notebook *v1alpha1.NoteBook
	var ins = &v1alpha1.NoteBook{}
	defer func() {
		h.WriteResponse(notebook, err, req, resp)
	}()
	if err = req.ReadEntity(ins); err != nil {
		return
	}
	// sync secret to business namespace
	regionK8sClient := abctx.Client(req.Request.Context())
	// render mlBinding
	mlBinding, err := h.projectBindingProcessor.GetMLBindingList(amlClient, req)
	if err != nil {
		return
	}
	// render storage
	err = h.projectBindingProcessor.ParseStorage(ins, mlBinding)
	if err != nil {
		return
	}
	notebook, err = h.Processor.CreateNotebook(req.Request.Context(), amlClient, regionK8sClient, namespace, ins)
	if err != nil {
		return
	}
}
func (h Handler) UpdateNotebook(req *restful.Request, resp *restful.Response) {
	var err error
	amlClient := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var notebook *v1alpha1.NoteBook
	var requestObj = &v1alpha1.NoteBook{}
	defer func() {
		h.WriteResponse(notebook, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	// sync secret to business namespace
	regionK8sClient := abctx.Client(req.Request.Context())
	notebook, err = h.Processor.UpdateNotebook(req.Request.Context(), amlClient, regionK8sClient, namespace, name, requestObj)
}
func (h Handler) DeleteNotebook(req *restful.Request, resp *restful.Response) {
	amlClient := localctx.AMLClient(req.Request.Context())
	regionK8sClient := abctx.Client(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	err = h.Processor.DeleteNotebook(req.Request.Context(), amlClient, regionK8sClient, namespace, name)
	if err != nil {
		return
	}
}

func (h Handler) StartNotebook(req *restful.Request, resp *restful.Response) {
	amlClient := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var notebook *v1alpha1.NoteBook
	var err error
	defer func() {
		h.WriteResponse(notebook, err, req, resp)
	}()
	notebook, err = h.Processor.StartNotebook(req.Request.Context(), amlClient, namespace, name)
}
func (h Handler) StopNotebook(req *restful.Request, resp *restful.Response) {
	amlClient := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var notebook *v1alpha1.NoteBook
	var err error
	defer func() {
		h.WriteResponse(notebook, err, req, resp)
	}()
	notebook, err = h.Processor.StopNotebook(req.Request.Context(), amlClient, namespace, name)
}
