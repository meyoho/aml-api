package notebook

import (
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	mlfake "alauda.io/amlopr/pkg/client/clientset/versioned/fake"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"

	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func InjectLogger() context.Context {
	return abctx.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Notebook", func() {
	var (
		ctx                    context.Context
		err                    error
		client                 *fake.Clientset
		mlClient               *mlfake.Clientset
		process                Processor
		wantedNamespace        string
		wantedName             string
		secretName             string
		notExistsInstanceName  string
		withSecretInstanceName string
		actual                 *v1alpha1.NoteBook
		runtimeObject          []runtime.Object
	)
	BeforeEach(func() {
		wantedNamespace = "test-namespace"
		wantedName = "test-notebook"
		notExistsInstanceName = "unsetName"

		withSecretInstanceName = "test-with-secret"
		secretName = "secret-name"
		runtimeObject = []runtime.Object{
			GetExpectNotebook(wantedNamespace, wantedName, ""),
			GetExpectNotebook(wantedNamespace, withSecretInstanceName, secretName),
		}
		mlClient = mlfake.NewSimpleClientset(runtimeObject...)
		client = fake.NewSimpleClientset(GetSecret(wantedNamespace, secretName))
		process = NewProcessor()
	})

	It("get a not exists notebook", func() {
		_, err = process.RetrieveNotebook(context.Background(), mlClient, wantedNamespace, notExistsInstanceName)
		Expect(err).To(HaveOccurred(), "should got a error.")
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("get an exists notebook", func() {
		actual, err = process.RetrieveNotebook(context.Background(), mlClient, wantedNamespace, wantedName)
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		Expect(actual.Namespace).To(Equal(wantedNamespace))
		Expect(actual.Name).To(Equal(wantedName))
	})
	It("delete a not existed notebook", func() {
		err = process.DeleteNotebook(context.Background(), mlClient, client, wantedNamespace, notExistsInstanceName)
		Expect(err).To(HaveOccurred(), "should got a error.")
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("delete an existed notebook without a secret", func() {
		err = process.DeleteNotebook(context.Background(), mlClient, client, wantedNamespace, wantedName)
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		_, err = process.RetrieveNotebook(context.Background(), mlClient, wantedNamespace, wantedName)
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("delete an existed notebook with a secret", func() {
		err = process.DeleteNotebook(context.Background(), mlClient, client, wantedNamespace, withSecretInstanceName)
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		_, err = process.RetrieveNotebook(context.Background(), mlClient, wantedNamespace, withSecretInstanceName)
		Expect(errors.IsNotFound(err)).To(BeTrue())
		_, err = client.CoreV1().Secrets(wantedNamespace).Get(secretName, metav1.GetOptions{})
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("list notebook", func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query := dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		list, err := process.ListNotebook(ctx, mlClient, query, common.NewSameNamespaceQuery(wantedNamespace))
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		Expect(list.ListMeta.TotalItems).To(Equal(2))
	})
})

func GetExpectNotebook(namespace, name, secret string) *v1alpha1.NoteBook {
	deploy := &v1alpha1.NoteBook{}
	deploy.Namespace = namespace
	deploy.Name = name
	deploy.Spec.Framework.ImagePullSecret = secret
	return deploy
}
func GetSecret(namespace, name string) *v1.Secret {
	secret := &v1.Secret{}
	secret.Namespace = namespace
	secret.Name = name
	return secret
}
