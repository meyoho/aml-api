package notebook_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestNotebook(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("notebook.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/handler/notebook", []Reporter{junitReporter})
}
