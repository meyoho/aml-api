package notebook

import (
	"alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	"fmt"
	v1 "k8s.io/api/core/v1"
	kerrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//var _ Processor = processor{}

func (p processor) ListNotebook(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *NotebookList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list Notebook end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   common.GetLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}
	list, err := client.MlV1alpha1().NoteBooks(namespace.ToRequestParam()).List(listOptions)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data = &NotebookList{
		Items:    make([]v1alpha1.NoteBook, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToNotebookSlice(result)
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

func ConvertToNotebookSlice(filtered []metav1.Object) (items []v1alpha1.NoteBook) {
	items = make([]v1alpha1.NoteBook, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.NoteBook); ok {
			items = append(items, *cm)
		}
	}
	return
}

//
func (p processor) RetrieveNotebook(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.NoteBook, error) {
	notebook, err := client.MlV1alpha1().NoteBooks(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.NoteBook{}, err
	}
	return notebook, nil
}

func syncImageSecret2RegionConditional(notebook *v1alpha1.NoteBook, regionClient kubernetes.Interface, namespace string) error {
	secretName := notebook.Spec.Framework.OriginSecretName
	secretNamespace := notebook.Spec.Framework.OriginSecretNamespace
	if secretName != "" && secretNamespace != "" {
		globalClient, err := common.GetGlobalClient()
		if err != nil {
			return err
		}
		globalSecret, err := globalClient.CoreV1().Secrets(secretNamespace).Get(secretName, metav1.GetOptions{})
		if err != nil {
			return err
		}
		foundSecret, err := regionClient.CoreV1().Secrets(namespace).Get(getSyncedSecretName(secretName), metav1.GetOptions{})
		secretTobeSynced := getCreateOrUpdateSecret(err, globalSecret, foundSecret, namespace)
		if err != nil {
			if kerrors.IsNotFound(err) {
				_, err = regionClient.CoreV1().Secrets(namespace).Create(secretTobeSynced)
				if err != nil {
					return err
				}
			} else {
				return err
			}
		} else {
			_, err = regionClient.CoreV1().Secrets(namespace).Update(secretTobeSynced)
			if err != nil {
				return err
			}
		}
		//add secret name to notebook cr
		notebook.Spec.Framework.ImagePullSecret = secretTobeSynced.Name
	}
	return nil
}

func deleteImageSecretInRegionConditional(amlClient versioned.Interface, regionClient kubernetes.Interface, namespace, notebookName string) error {
	notebook, err := amlClient.MlV1alpha1().NoteBooks(namespace).Get(notebookName, metav1.GetOptions{})
	if err != nil {
		return err
	}
	secretName := notebook.Spec.Framework.ImagePullSecret
	if secretName != "" {
		return regionClient.CoreV1().Secrets(namespace).Delete(secretName, &metav1.DeleteOptions{})
	}
	return nil
}

func (p processor) CreateNotebook(ctx context.Context, client versioned.Interface, regionClient kubernetes.Interface, namespace string, requestObj *v1alpha1.NoteBook) (*v1alpha1.NoteBook, error) {
	ins := &v1alpha1.NoteBook{}
	err := syncImageSecret2RegionConditional(requestObj, regionClient, namespace)
	if err != nil {
		return ins, err
	}
	return client.MlV1alpha1().NoteBooks(namespace).Create(requestObj)
}

func (p processor) UpdateNotebook(ctx context.Context, client versioned.Interface, regionClient kubernetes.Interface, namespace, name string, requestObj *v1alpha1.NoteBook) (*v1alpha1.NoteBook, error) {
	fromObj, err := client.MlV1alpha1().NoteBooks(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.NoteBook{}, err
	}
	err = syncImageSecret2RegionConditional(requestObj, regionClient, namespace)
	if err != nil {
		return &v1alpha1.NoteBook{}, err
	}
	to := fromObj.DeepCopy()
	to.ObjectMeta.Annotations = requestObj.ObjectMeta.Annotations
	if requestObj.ObjectMeta.Labels != nil {
		to.ObjectMeta.Labels = requestObj.ObjectMeta.Labels
	}
	to.Spec.Quota = requestObj.Spec.Quota
	to.Spec.Framework = requestObj.Spec.Framework
	to.Spec.Framework.OriginSecretName = requestObj.Spec.Framework.OriginSecretName
	to.Spec.Framework.OriginSecretNamespace = requestObj.Spec.Framework.OriginSecretNamespace
	to.Spec.Framework.ImagePullSecret = getSyncedSecretName(requestObj.Spec.Framework.OriginSecretName)
	to.Status.State = v1alpha1.StateInitializing
	return client.MlV1alpha1().NoteBooks(namespace).Update(to)
}

func (p processor) DeleteNotebook(ctx context.Context, client versioned.Interface, regionClient kubernetes.Interface, namespace, name string) error {
	err := deleteImageSecretInRegionConditional(client, regionClient, namespace, name)
	if err != nil && !errors.IsNotFoundError(err) {
		return err
	}
	return client.MlV1alpha1().NoteBooks(namespace).Delete(name, &metav1.DeleteOptions{})
}
func (p processor) StartNotebook(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.NoteBook, error) {
	oldNb, err := client.MlV1alpha1().NoteBooks(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.NoteBook{}, err
	}
	if oldNb.Status.State != v1alpha1.StateStopped {
		return &v1alpha1.NoteBook{}, kerrors.NewBadRequest(fmt.Sprintf("current state is not %v, can not run to start state(%v)",
			v1alpha1.StateStopped, v1alpha1.StateInitializing))
	}
	to := oldNb.DeepCopy()
	to.Status.State = v1alpha1.StateInitializing
	return client.MlV1alpha1().NoteBooks(namespace).Update(to)
}
func (p processor) StopNotebook(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.NoteBook, error) {
	oldNb, err := client.MlV1alpha1().NoteBooks(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.NoteBook{}, err
	}
	if oldNb.Status.State == v1alpha1.StateInitializedError {
		return &v1alpha1.NoteBook{}, kerrors.NewBadRequest(fmt.Sprintf("current state is %s, can not run to stop state",
			v1alpha1.StateInitializedError))
	}
	to := oldNb.DeepCopy()
	to.Status.State = v1alpha1.StateStopping
	return client.MlV1alpha1().NoteBooks(namespace).Update(to)
}
func getSyncedSecretName(originName string) string {
	if len(originName) > 0 {
		return fmt.Sprintf("ml-copy-%v", originName)
	}
	return ""
}
func getCreateOrUpdateSecret(err error, globalSecret, foundSecret *v1.Secret, namespace string) *v1.Secret {
	var tobeSynced *v1.Secret
	if err == nil {
		// update secret
		tobeSynced = foundSecret.DeepCopy()
	} else {
		//create secret
		tobeSynced = &v1.Secret{}
		tobeSynced.Namespace = namespace
		tobeSynced.Name = getSyncedSecretName(globalSecret.Name)
	}
	tobeSynced.Data = globalSecret.Data
	tobeSynced.StringData = globalSecret.StringData
	return tobeSynced
}
