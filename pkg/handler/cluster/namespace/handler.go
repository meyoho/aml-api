package namespace

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	processor Processor
}

func NewHandler(svr server.Server) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		processor:       Processor{},
	}
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/ml/namespaces")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "mlNamespace")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{cluster}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List namespace detail`).
					To(handler.ListNamespaceDetail).
					Writes(List{}).
					Returns(http.StatusOK, "OK", List{}),
			),
		),
	)

	ws.Filter(clientFilter.AMLClientFilter)
	return
}

func (h Handler) ListNamespaceDetail(req *restful.Request, resp *restful.Response) {
	mlClient := localctx.AMLClient(req.Request.Context())
	k8sClient := localctx.InsecureClient(req.Request.Context())
	query := abctx.Query(req.Request.Context())
	list, err := h.processor.ListNameSpace(k8sClient, mlClient, query)
	h.WriteResponse(list, err, req, resp)
}
