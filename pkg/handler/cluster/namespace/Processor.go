package namespace

import (
	"alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type Processor struct {
}

func (p Processor) ListNameSpace(client kubernetes.Interface, mlClient versioned.Interface, query *dataselect.Query) (data *List, err error) {
	listOptions := metav1.ListOptions{
		LabelSelector: common.GetLabelSelectorByDsQuery(query).String(),
		//ResourceVersion: "0",
	}
	list, err := client.CoreV1().Namespaces().List(listOptions)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data = &List{
		Items:    make([]Detail, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items, err = ConvertToDetailSlice(mlClient, result)
	nonCriticalErrors, criticalError = errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

func ConvertToDetailSlice(mlClient versioned.Interface, filtered []metav1.Object) (items []Detail, err error) {
	items = make([]Detail, 0, len(filtered))
	for _, item := range filtered {
		if namespace, ok := item.(*v1.Namespace); ok {
			detail := Detail{}
			detail.Namespace = *namespace
			bindingList, err := mlClient.MlV1alpha1().MLBindings(namespace.Name).List(metav1.ListOptions{})
			if err != nil {
				detail.Err = err.Error()
				items = append(items, detail)
				continue
			}
			if len(bindingList.Items) < 1 {
				detail.MLBinding = nil
			} else {
				detail.MLBinding = &bindingList.Items[0]
			}

			items = append(items, detail)
		}
	}
	return
}
