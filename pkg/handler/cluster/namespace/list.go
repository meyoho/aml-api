package namespace

import (
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"k8s.io/api/core/v1"
)

// PipelineList contains a list of jenkins in the cluster.
type List struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of Pipeline
	Items []Detail `json:"details"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type Detail struct {
	Namespace v1.Namespace        `json:"namespace"`
	MLBinding *v1alpha1.MLBinding `json:"amlprojectbinding"`
	Err       string              `json:"err"`
}
