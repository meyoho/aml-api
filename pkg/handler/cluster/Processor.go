package cluster

import (
	mlbase "alauda.io/aml-api/pkg/base"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/aml-api/pkg/handler/devops/jenkinsbinding"
	"alauda.io/amlopr/pkg/apis/base"
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"github.com/emicklei/go-restful"
	"github.com/juju/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
)

type processor struct {
	req *restful.Request
	mgr client.Manager
}

func newProcessor(mgr client.Manager, req *restful.Request) processor {
	return processor{req, mgr}
}

var Schema = &schema.GroupVersionKind{
	Group:   "clusterregistry.k8s.io",
	Kind:    "Cluster",
	Version: "v1alpha1",
}

func (p processor) ListCluster(namespace string, query *dataselect.Query, fieldSelector string) (*List, error) {
	clusterClient, err := p.getClusterClient(mlbase.GetAlaudaNamespace())
	if err != nil {
		return nil, errors.Trace(err)
	}

	listOptions := metav1.ListOptions{
		LabelSelector: common.GetLabelSelectorByDsQuery(query).String(),
		FieldSelector: fieldSelector,
	}
	unstructuredList, err := clusterClient.List(listOptions)
	itemCells := dataselect.ToObjectCellSlice(unstructuredList.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)
	return p.convertToDetailSlice(result, namespace, filteredTotal), nil
}

func (p processor) convertToDetailSlice(filtered []metav1.Object, namespace string, totalItems int) (result *List) {
	result = &List{ListMeta: common.ListMeta{TotalItems: totalItems}}
	for _, item := range filtered {
		if cluster, ok := item.(*unstructured.Unstructured); ok {
			detail := &Detail{}
			detail.Cluster = cluster
			JenkinsBindingList, err := p.getJenkinsBindingList(cluster, namespace)
			if err != nil {
				result.Errors = append(result.Errors, err)
			}
			detail.JenkinsBindings = JenkinsBindingList.Items
			result.Items = append(result.Items, *detail)
		}
	}
	return
}

func (p processor) getJenkinsBindingList(cluster *unstructured.Unstructured, namespace string) (*unstructured.UnstructuredList, error) {
	selectorString := base.GetMLJenkinsBindingLabel(cluster.GetName()).GetSelectorString()
	options := metav1.ListOptions{
		LabelSelector: selectorString,
	}
	return p.getJBProcessor().ListJenkinsBinding(namespace, options)
}

func (p processor) getJBProcessor() jenkinsbinding.Processor {
	return jenkinsbinding.NewProcessor(p.mgr, p.req)
}

func (p processor) getClusterClient(namespace string) (dynamic.ResourceInterface, error) {
	dynamicClient, err := p.mgr.DynamicClient(p.req, Schema)
	if err != nil {
		return nil, errors.Trace(err)
	}

	return dynamicClient.Namespace(namespace), nil
}
