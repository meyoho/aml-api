package cluster

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/aml-api/pkg/handler/configmap"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"github.com/emicklei/go-restful"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	v12 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
)

const ProjectConfigName = "ml-project-config"
const DefaultClusterKey = "defaultJenkinsCluster"

type Handler struct {
	*common.AbstractHandler
	configmapProcessor configmap.Processor
}

func NewHandler(svr server.Server) Handler {
	return Handler{
		AbstractHandler:    &common.AbstractHandler{Server: svr},
		configmapProcessor: configmap.Processor{},
	}
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/clusters")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "defaultclusters")))
	ws.Route(
		queryFilter.Build(
			abdecorator.WithAuth(
				ws.GET("/{namespace}").
					Param(ws.PathParameter("namespace", "namespace")).
					Filter(clientFilter.AMLClientFilter).
					Doc("get cluster").
					To(handler.ListCluster).
					Returns(http.StatusOK, "ok", ""),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/default/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get configmap").
				Param(ws.PathParameter("namespace", "namespace")).
				To(handler.GetDefaultCluster).
				Returns(http.StatusOK, "ok", ""),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/default/{namespace}").
				Filter(clientFilter.AMLClientFilter).
				Doc("update default cluster").
				Param(ws.PathParameter("namespace", "namespace")).
				To(handler.UpdateDefaultCluster).
				Returns(http.StatusOK, "ok", ""),
		),
	)

	ws.Filter(clientFilter.AMLClientFilter)
	return
}

func (h Handler) ListCluster(req *restful.Request, resp *restful.Response) {
	query := abctx.Query(req.Request.Context())
	namespace := req.PathParameter("namespace")
	fieldSelector := req.QueryParameter("fieldSelector")
	list, err := h.getProcessor(req).ListCluster(namespace, query, fieldSelector)
	h.WriteResponse(list, err, req, resp)
}

func (h Handler) GetDefaultCluster(req *restful.Request, resp *restful.Response) {
	client := localctx.InsecureClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	configMap, err := h.configmapProcessor.GetConfigMap(client, namespace, "ml-project-config")
	if err != nil && errors.IsNotFound(err) {
		h.WriteResponse("", nil, req, resp)
		return
	}

	h.WriteResponse(configMap.Data[DefaultClusterKey], err, req, resp)
	return
}

func (h Handler) UpdateDefaultCluster(req *restful.Request, resp *restful.Response) {
	client := localctx.InsecureClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	var result *v1.ConfigMap
	var defaultClusterName = ""
	var err error
	if err = req.ReadEntity(&defaultClusterName); err != nil {
		return
	}

	old, err := h.configmapProcessor.GetConfigMap(client, namespace, ProjectConfigName)
	if err != nil {
		if errors.IsNotFound(err) {
			created := &v1.ConfigMap{
				ObjectMeta: v12.ObjectMeta{
					Name:      ProjectConfigName,
					Namespace: namespace,
				},
				Data: map[string]string{DefaultClusterKey: defaultClusterName},
			}
			result, err = h.configmapProcessor.CreateConfigMap(client, namespace, created)
			h.WriteResponse(result.Data[DefaultClusterKey], err, req, resp)
			return
		}
		h.WriteResponse("", err, req, resp)
	}

	old.Data[DefaultClusterKey] = defaultClusterName
	result, err = h.configmapProcessor.UpdateConfigMap(client, namespace, ProjectConfigName, old)
	h.WriteResponse(result.Data[DefaultClusterKey], err, req, resp)
}

func (h Handler) getProcessor(req *restful.Request) processor {
	return newProcessor(h.Server.GetManager(), req)
}
