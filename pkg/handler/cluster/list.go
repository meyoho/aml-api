package cluster

import (
	"alauda.io/aml-api/pkg/handler/common"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

// PipelineList contains a list of jenkins in the cluster.
type List struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of Pipeline
	Items []Detail `json:"details"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}

type Detail struct {
	Cluster         *unstructured.Unstructured  `json:"cluster"`
	JenkinsBindings []unstructured.Unstructured `json:"jenkinsBindings"`
}
