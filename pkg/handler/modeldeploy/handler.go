package modeldeploy

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	"context"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
	Processor Processor
}

func NewHandler(svr server.Server, processor Processor) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
		Processor:       processor,
	}
}

type Processor interface {
	RetrieveModelDeploy(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.ModelDeployment, error)
	DeleteModelDeploy(ctx context.Context, amlClient versioned.Interface, client kubernetes.Interface, namespace, modelDeployName string) error
	ListModelDeploy(ctx context.Context, amlClient versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (*ModelDeployList, error)
	UpdateModelDeploy(ctx context.Context, amlClient versioned.Interface, regionClient kubernetes.Interface, namespace, name string, notebook *v1alpha1.ModelDeployment) (*v1alpha1.ModelDeployment, error)
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr, NewProcessor())
	clientFilter := decorator.ClientDecorator(svr)
	queryFilter := abdecorator.NewQuery()
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/modeldeployments")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "modeldeployments")))
	ws.Route(
		// queryFilter adds all parameters for documentation
		// and injects a Query Builder filter as a middleware to enable
		// parsing request data and building a *dataselect.Query object into Context
		queryFilter.Build(
			// this method will add a few response types according to different http status
			// to the documentation
			abdecorator.WithAuth(
				ws.GET("/{cluster}/{namespace}").
					Filter(clientFilter.AMLClientFilter).
					Doc(`List Notebook instance`).
					Param(ws.PathParameter("cluster", "cluster of the notebook")).
					Param(ws.PathParameter("namespace", "namespace of the notebook")).
					To(handler.ListModelDeploy).
					Writes(v1alpha1.NoteBook{}).
					Returns(http.StatusOK, "OK", ModelDeployList{}),
			),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.AMLClientFilter).
				Doc("get a modeldeployment").
				Param(ws.PathParameter("cluster", "cluster of the modeldeployment")).
				Param(ws.PathParameter("namespace", "namespace of the modeldeployment")).
				Param(ws.PathParameter("name", "name of the modeldeployment")).
				To(handler.RetrieveModelDeploy).
				Returns(http.StatusOK, "ok", v1alpha1.ModelDeployment{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.DELETE("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AMLClientFilter).
				Doc("delete a modeldeployment").
				Param(ws.PathParameter("cluster", "cluster of the modeldeployment")).
				Param(ws.PathParameter("namespace", "namespace of the modeldeployment")).
				Param(ws.PathParameter("name", "name of the modeldeployment")).
				To(handler.DeleteModelDeploy).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Route(
		abdecorator.WithAuth(
			ws.PUT("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Filter(clientFilter.AMLClientFilter).
				Doc("update a modeldeployment").
				Param(ws.PathParameter("cluster", "cluster of the modeldeployment")).
				Param(ws.PathParameter("namespace", "namespace of the modeldeployment")).
				Param(ws.PathParameter("name", "name of the modeldeployment")).
				To(handler.UpdateModelDeploy).
				Returns(http.StatusOK, "ok", v1alpha1.NoteBook{}),
		),
	)
	ws.Filter(clientFilter.AMLClientFilter)
	return
}
func (h Handler) RetrieveModelDeploy(req *restful.Request, resp *restful.Response) {
	client := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	notebook, err := h.Processor.RetrieveModelDeploy(req.Request.Context(), client, namespace, name)
	h.WriteResponse(notebook, err, req, resp)
	return
}
func (h Handler) DeleteModelDeploy(req *restful.Request, resp *restful.Response) {
	amlClient := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var err error
	defer func() {
		h.WriteResponse("", err, req, resp)
	}()
	regionK8sClient := abctx.Client(req.Request.Context())
	err = h.Processor.DeleteModelDeploy(req.Request.Context(), amlClient, regionK8sClient, namespace, name)
}
func (h Handler) ListModelDeploy(req *restful.Request, resp *restful.Response) {
	amlClient := localctx.AMLClient(req.Request.Context())
	query := abctx.Query(req.Request.Context())
	namespace := common.ParseNamespacePathParameter(req)
	list, err := h.Processor.ListModelDeploy(req.Request.Context(), amlClient, query, namespace)
	h.WriteResponse(list, err, req, resp)
}

func (h Handler) UpdateModelDeploy(req *restful.Request, resp *restful.Response) {
	var err error
	amlClient := localctx.AMLClient(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")
	var modelDeploy *v1alpha1.ModelDeployment
	var requestObj = &v1alpha1.ModelDeployment{}
	defer func() {
		h.WriteResponse(modelDeploy, err, req, resp)
	}()
	if err = req.ReadEntity(requestObj); err != nil {
		return
	}
	regionK8sClient := abctx.Client(req.Request.Context())
	modelDeploy, err = h.Processor.UpdateModelDeploy(req.Request.Context(), amlClient, regionK8sClient, namespace, name, requestObj)
}
