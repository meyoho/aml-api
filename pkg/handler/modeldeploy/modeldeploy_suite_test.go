package modeldeploy_test

import (
	"github.com/onsi/ginkgo/reporters"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestModeldeploy(t *testing.T) {
	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("model_deployment.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "pkg/handler/modeldeploy", []Reporter{junitReporter})
}
