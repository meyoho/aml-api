package modeldeploy

import (
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	mlfake "alauda.io/amlopr/pkg/client/clientset/versioned/fake"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"context"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"go.uber.org/zap"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"

	abctx "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func InjectLogger() context.Context {
	return abctx.WithLogger(context.TODO(), zap.NewExample())
}

var _ = Describe("Model Deploy", func() {
	var (
		ctx                      context.Context
		err                      error
		client                   *fake.Clientset
		mlClient                 *mlfake.Clientset
		process                  Processor
		wantedNamespace          string
		wantedName               string
		secretName               string
		notExistsInstanceName    string
		withSecretInstanceName   string
		actualDeploy             *v1alpha1.ModelDeployment
		modelDeployRuntimeObject []runtime.Object
	)
	BeforeEach(func() {
		wantedNamespace = "test-namespace"
		wantedName = "test-model-deploy"
		notExistsInstanceName = "unsetName"

		withSecretInstanceName = "test-with-secret"
		secretName = "secret-name"
		modelDeployRuntimeObject = []runtime.Object{
			GetExpectModelDeploy(wantedNamespace, wantedName, ""),
			GetExpectModelDeploy(wantedNamespace, withSecretInstanceName, secretName),
		}
		mlClient = mlfake.NewSimpleClientset(modelDeployRuntimeObject...)
		client = fake.NewSimpleClientset(GetSecret(wantedNamespace, secretName))
		process = NewProcessor()
	})
	It("get a not exists model deploy", func() {
		_, err = process.RetrieveModelDeploy(context.Background(), mlClient, wantedNamespace, notExistsInstanceName)
		Expect(err).To(HaveOccurred(), "should got a error.")
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("get an exists model deploy", func() {
		//_, err := mlClient.MlV1alpha1().ModelDeployments(wantedNamespace).Get(wantedName, metav1.GetOptions{})
		actualDeploy, err = process.RetrieveModelDeploy(context.Background(), mlClient, wantedNamespace, wantedName)
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		Expect(actualDeploy.Namespace).To(Equal(wantedNamespace))
		Expect(actualDeploy.Name).To(Equal(wantedName))
	})
	It("delete a not existed model deploy", func() {
		err = process.DeleteModelDeploy(context.Background(), mlClient, client, wantedNamespace, notExistsInstanceName)
		Expect(err).To(HaveOccurred(), "should got a error.")
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("delete an existed model deploy without a secret", func() {
		err = process.DeleteModelDeploy(context.Background(), mlClient, client, wantedNamespace, wantedName)
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		_, err = process.RetrieveModelDeploy(context.Background(), mlClient, wantedNamespace, wantedName)
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("delete an existed model deploy with a secret", func() {
		err = process.DeleteModelDeploy(context.Background(), mlClient, client, wantedNamespace, withSecretInstanceName)
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		_, err = process.RetrieveModelDeploy(context.Background(), mlClient, wantedNamespace, withSecretInstanceName)
		Expect(errors.IsNotFound(err)).To(BeTrue())
		_, err = client.CoreV1().Secrets(wantedNamespace).Get(secretName, metav1.GetOptions{})
		Expect(errors.IsNotFound(err)).To(BeTrue())
	})
	It("list model deploy", func() {
		ctx = InjectLogger()
		defaultPagequery := &dataselect.PaginationQuery{
			ItemsPerPage: 10,
			Page:         0,
		}
		defaultSortQuery := &dataselect.SortQuery{
			SortByList: []dataselect.SortBy{},
		}
		defaultFilter := &dataselect.FilterQuery{
			FilterByList: []dataselect.FilterBy{},
		}
		query := dataselect.NewDataSelectQuery(defaultPagequery, defaultSortQuery, defaultFilter)
		list, err := process.ListModelDeploy(ctx, mlClient, query, common.NewSameNamespaceQuery(wantedNamespace))
		Expect(err).ToNot(HaveOccurred(), "should not got a error.")
		Expect(list.ListMeta.TotalItems).To(Equal(2))
	})

})

func GetExpectModelDeploy(namespace, name, secret string) *v1alpha1.ModelDeployment {
	deploy := &v1alpha1.ModelDeployment{}
	deploy.Namespace = namespace
	deploy.Name = name
	deploy.Spec.ImageRepository.Credential.Id = secret
	return deploy
}
func GetSecret(namespace, name string) *v1.Secret {
	secret := &v1.Secret{}
	secret.Namespace = namespace
	secret.Name = name
	return secret
}
