package modeldeploy

import (
	"alauda.io/aml-api/pkg/errors"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	abcontext "bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"bitbucket.org/mathildetech/log"
	"context"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type processor struct {
}

// NewProcessor constructor for internal processor
func NewProcessor() Processor {
	return processor{}
}

//
func (p processor) ListModelDeploy(ctx context.Context, client versioned.Interface, query *dataselect.Query, namespace *common.NamespaceQuery) (data *ModelDeployList, err error) {
	logger := abcontext.Logger(ctx)
	defer func() {
		logger.Debug("list Modeldeployment end", log.Any("query", query), log.Any("list", data), log.Err(err))
	}()
	listOptions := metav1.ListOptions{
		LabelSelector:   common.GetLabelSelectorByDsQuery(query).String(),
		ResourceVersion: "0",
	}
	list, err := client.MlV1alpha1().ModelDeployments(namespace.ToRequestParam()).List(listOptions)
	nonCriticalErrors, criticalError := errors.HandleError(err)
	if criticalError != nil {
		return nil, criticalError
	}
	data = &ModelDeployList{
		Items:    make([]v1alpha1.ModelDeployment, 0),
		ListMeta: common.ListMeta{TotalItems: 0},
	}
	itemCells := dataselect.ToObjectCellSlice(list.Items)
	itemCells, filteredTotal := dataselect.GenericDataSelectWithFilter(itemCells, query)
	result := dataselect.FromCellToObjectSlice(itemCells)

	data.Items = ConvertToSlice(result)
	data.ListMeta = common.ListMeta{TotalItems: filteredTotal}
	data.Errors = nonCriticalErrors
	return
}

func ConvertToSlice(filtered []metav1.Object) (items []v1alpha1.ModelDeployment) {
	items = make([]v1alpha1.ModelDeployment, 0, len(filtered))
	for _, item := range filtered {
		if cm, ok := item.(*v1alpha1.ModelDeployment); ok {
			items = append(items, *cm)
		}
	}
	return
}

func (p processor) RetrieveModelDeploy(ctx context.Context, client versioned.Interface, namespace, name string) (*v1alpha1.ModelDeployment, error) {
	notebook, err := client.MlV1alpha1().ModelDeployments(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return &v1alpha1.ModelDeployment{}, err
	}
	return notebook, nil
}
func (p processor) DeleteModelDeploy(ctx context.Context, amlClient versioned.Interface, client kubernetes.Interface, namespace, modelDeployName string) error {
	var err error
	err = deleteImageSecretInRegionConditional(amlClient, client, namespace, modelDeployName)
	if err != nil && !errors.IsNotFoundError(err) {
		return err
	}
	return deleteModelDeploy(ctx, amlClient, namespace, modelDeployName)
}
func deleteModelDeploy(ctx context.Context, client versioned.Interface, namespace, name string) error {
	return client.MlV1alpha1().ModelDeployments(namespace).Delete(name, &metav1.DeleteOptions{})
}

func deleteImageSecretInRegionConditional(amlClient versioned.Interface, regionClient kubernetes.Interface, namespace, modelDeployName string) error {
	modelDeploy, err := amlClient.MlV1alpha1().ModelDeployments(namespace).Get(modelDeployName, metav1.GetOptions{})
	if err != nil {
		return err
	}
	secretName := modelDeploy.Spec.ImageRepository.Credential.Id
	if secretName != "" {
		return regionClient.CoreV1().Secrets(namespace).Delete(secretName, &metav1.DeleteOptions{})
	}
	return nil
}
func (p processor) UpdateModelDeploy(ctx context.Context, amlClient versioned.Interface, regionClient kubernetes.Interface, namespace, name string, modelDeploy *v1alpha1.ModelDeployment) (*v1alpha1.ModelDeployment, error) {
	existModelDeployment, err := amlClient.MlV1alpha1().ModelDeployments(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	existModelDeployment.Spec.DeployInfo.Replica = modelDeploy.Spec.DeployInfo.Replica
	existModelDeployment.Spec.DeployInfo.Resource = modelDeploy.Spec.DeployInfo.Resource
	//TODO fy 更新镜像，同步secret
	/*
		if ! (modelDeploy.Spec.ImageRepository.OriginSecretName == existModelDeployment.Spec.ImageRepository.OriginSecretName &&
			modelDeploy.Spec.ImageRepository.OriginSecretNamespace == existModelDeployment.Spec.ImageRepository.OriginSecretNamespace) {
			// sync secret from global to region

			//set value
		}
	*/
	existModelDeployment.Status.State = v1alpha1.StateModelDeploymentUpdating
	return amlClient.MlV1alpha1().ModelDeployments(namespace).Update(existModelDeployment)

}
