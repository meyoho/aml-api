package modeldeploy

import (
	//"alauda.io/devops-apiserver/pkg/apis/devops/v1alpha1"
	"alauda.io/aml-api/pkg/handler/common"
	"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
)

// PipelineList contains a list of jenkins in the cluster.
type ModelDeployList struct {
	ListMeta common.ListMeta `json:"listMeta"`

	// Unordered list of Pipeline.
	Items []v1alpha1.ModelDeployment `json:"modeldeployments"`

	// List of non-critical errors, that occurred during resource retrieval.
	Errors []error `json:"errors"`
}
