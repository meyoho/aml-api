package handler

import (
	"alauda.io/aml-api/pkg/handler/cluster"
	"alauda.io/aml-api/pkg/handler/cluster/namespace"
	"alauda.io/aml-api/pkg/handler/configmap"
	"alauda.io/aml-api/pkg/handler/devops/jenkinsbinding"
	"alauda.io/aml-api/pkg/handler/images/system"
	"alauda.io/aml-api/pkg/handler/modeldeploy"
	"alauda.io/aml-api/pkg/handler/modelservice"
	"alauda.io/aml-api/pkg/handler/notebook"
	"alauda.io/aml-api/pkg/handler/pipeline/pipeline"
	"alauda.io/aml-api/pkg/handler/pipeline/pipelineconfig"
	"alauda.io/aml-api/pkg/handler/pipeline/pipelinetemplate"
	"alauda.io/aml-api/pkg/handler/pod"
	"alauda.io/aml-api/pkg/handler/projectbinding"
	"alauda.io/aml-api/pkg/handler/quota"
	"alauda.io/aml-api/pkg/handler/training/dataviewer"
	"alauda.io/aml-api/pkg/handler/training/pytorch"
	"alauda.io/aml-api/pkg/handler/training/pytorch/pytorchjob"
	"alauda.io/aml-api/pkg/handler/training/tensorflow"
	"alauda.io/aml-api/pkg/handler/training/tensorflow/tfjob"
	"bitbucket.org/mathildetech/alauda-backend/pkg/registry"
)

func init() {
	println("------ registry --------")
	registry.AddBuilder(notebook.New)
	registry.AddBuilder(modeldeploy.New)

	registry.AddBuilder(tensorflow.New)
	registry.AddBuilder(tfjob.NewJob)
	registry.AddBuilder(pytorch.New)
	registry.AddBuilder(pytorchjob.NewJob)
	registry.AddBuilder(pod.New)
	registry.AddBuilder(dataviewer.New)

	registry.AddBuilder(pipelinetemplate.New)
	registry.AddBuilder(pipelineconfig.New)
	registry.AddBuilder(pipeline.New)

	registry.AddBuilder(projectbinding.New)
	//images
	registry.AddBuilder(system.New)

	registry.AddBuilder(configmap.New)

	registry.AddBuilder(namespace.New)
	registry.AddBuilder(cluster.New)
	registry.AddBuilder(jenkinsbinding.New)

	registry.AddBuilder(quota.New)
	registry.AddBuilder(modelservice.New)

}
