package pod

import (
	"k8s.io/api/core/v1"
)

type PodDetail struct {
	Metrics   PodMetrics `json:"metrics,omitempty"`
	Data      v1.Pod     `json:"data"`
	PodStatus PodStatus  `json:"podStatus"`
	Error     string     `json:"error"`
}
type PodStatus struct {
	Status          string              `json:"status"`
	PodPhase        v1.PodPhase         `json:"podPhase"`
	ContainerStates []v1.ContainerState `json:"containerStates"`
}
type PodMetrics struct {
	CPU    int64   `json:"cpu"`
	Memory int64   `json:"memory"`
	Gpu    float64 `json:"gpu"`
}
