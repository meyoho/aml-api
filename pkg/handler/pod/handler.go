package pod

import (
	//localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/decorator"
	"alauda.io/aml-api/pkg/handler/common"
	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	//"alauda.io/amlopr/pkg/apis/ml/v1alpha1"
	//"alauda.io/amlopr/pkg/client/clientset/versioned"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	//"context"
	"github.com/emicklei/go-restful"
	"k8s.io/api/core/v1"
	"net/http"
)

type Handler struct {
	*common.AbstractHandler
}

func NewHandler(svr server.Server) Handler {
	return Handler{
		AbstractHandler: &common.AbstractHandler{Server: svr},
	}
}

func New(svr server.Server) (ws *restful.WebService, err error) {
	handler := NewHandler(svr)
	clientFilter := decorator.ClientDecorator(svr)
	loggerFilter := decorator.LoggerDecorator(svr)
	ws = abdecorator.NewWebService(svr)
	ws.Doc("").ApiVersion("v1").Path("/api/v1/logs")
	// adds a basic filter to add url path to the logger context
	ws.Filter(loggerFilter.Filter)
	ws.Filter(loggerFilter.CustomFilter(log.String("resource", "podlogs")))
	ws.Route(
		abdecorator.WithAuth(
			ws.GET("/{cluster}/{namespace}/{name}").
				Filter(clientFilter.SecureFilter).
				Doc("get a pod log").
				Param(ws.PathParameter("cluster", "cluster of the pod")).
				Param(ws.PathParameter("namespace", "namespace of the pod")).
				Param(ws.PathParameter("name", "name of the pod")).
				To(handler.RetrieveLog).
				Returns(http.StatusOK, "ok", struct{}{}),
		),
	)
	ws.Filter(clientFilter.SecureFilter)
	return
}

func (h Handler) RetrieveLog(req *restful.Request, resp *restful.Response) {
	client := context.Client(req.Request.Context())
	namespace := req.PathParameter("namespace")
	name := req.PathParameter("name")

	logs, err := client.CoreV1().Pods(namespace).GetLogs(name, &v1.PodLogOptions{}).Do().Raw()
	if err != nil {
		log.Warnf("failed to get pod logs for TFJob %v under namespace %v: %v", name, namespace, err)
		if err2 := resp.WriteError(http.StatusInternalServerError, err); err2 != nil {
			log.Errorf("Failed to write response: %v", err2)
		}
	} else {
		log.Infof("successfully get pod logs for TFJob %v under namespace %v", name, namespace)
		if err = resp.WriteHeaderAndEntity(http.StatusOK, string(logs)); err != nil {
			log.Errorf("Failed to write response: %v", err)
		}
	}
	return
}
