package pod

import (
	localctx "alauda.io/aml-api/pkg/context"
	"alauda.io/aml-api/pkg/handler/metrics"
	"bitbucket.org/mathildetech/alauda-backend/pkg/client"
	"github.com/emicklei/go-restful"
	"github.com/juju/errors"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type Processor struct {
	req *restful.Request
	mgr client.Manager
}

// NewProcessor constructor for internal Processor
func NewProcessor(mgr client.Manager, req *restful.Request) Processor {
	return Processor{req, mgr}
}

func (p Processor) ListPods(namespace string, listOptions metav1.ListOptions) (*v1.PodList, error) {
	insecureClient := localctx.InsecureClient(p.req.Request.Context())
	return insecureClient.CoreV1().Pods(namespace).List(listOptions)
}

//TODO we don't have metrics now ,so find a way complete the pod details
func (p Processor) GetPodDetails(pods *v1.PodList) ([]PodDetail, error) {
	var podDetails []PodDetail
	for _, pod := range pods.Items {
		metric, err := p.getPodMetrics(pod)
		errorString := ""
		if err != nil {
			errorString = err.Error()
		}
		podDetail := PodDetail{
			Data:      pod,
			PodStatus: GetPodStatus(&pod),
			Metrics:   metric,
			Error:     errorString,
		}
		podDetails = append(podDetails, podDetail)
	}
	return podDetails, nil
}

func (p Processor) getPodMetrics(pod v1.Pod) (PodMetrics, error) {
	metric := PodMetrics{}

	cluster := p.req.PathParameter("cluster")
	podCPU, err := getMetricsProcessor(p).GetPodCPU(cluster, pod.Namespace, pod.Name)
	if err != nil {
		return metric, errors.Trace(err)
	}
	metric.CPU = podCPU

	podMem, err := getMetricsProcessor(p).GetPodMem(cluster, pod.Namespace, pod.Name)
	if err != nil {
		return metric, errors.Trace(err)
	}
	metric.Memory = podMem

	podNvidiaGpuUtilization, err := getMetricsProcessor(p).GetPodNvidiaGpuUtilization(cluster, pod.Namespace, pod.Name)
	if err != nil {
		return metric, errors.Trace(err)
	}
	metric.Gpu = podNvidiaGpuUtilization
	return metric, nil
}

func getMetricsProcessor(p Processor) metrics.Processor {
	return metrics.NewProcessor(p.mgr, p.req)
}
