package common

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
)

type AbstractHandler struct {
	Server server.Server
}

func (h AbstractHandler) WriteResponse(data interface{}, err error, req *restful.Request, resp *restful.Response) {
	if err != nil {
		h.Server.HandleError(err, req, resp)
		return
	}
	resp.WriteAsJson(data)
}
