package env

import "os"

const (
	CMPrivateImagesKey      = "CM_PRIVATE_IMAGES_KEY"
	CMPrivateImagesScopeKey = "CM_PRIVATE_IMAGES_SCOPE_KEY"
	CMPrivateRegistryKey    = "CM_PRIVATE_Registry_KEY"
	Namespace               = "NAMESPACE"
	ConfigMapName           = "CONFIGMAP_NAME"
)

func GetEnv(key, fallback string) string {
	found, ok := os.LookupEnv(key)
	if !ok {
		return fallback
	}
	return found
}
