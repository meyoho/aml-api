package common

import (
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/controller-runtime/pkg/client/config"
)

func getConfig() (*rest.Config, error) {
	cfg, err := config.GetConfig()
	if err != nil {
		cfg, err = clientcmd.BuildConfigFromFlags("", "~/.kube/config")
	}
	return cfg, err
}
func GetGlobalClient() (kubernetes.Interface, error) {
	cfg, err := getConfig()
	if err != nil {
		return nil, err
	}
	return kubernetes.NewForConfig(cfg)
}
