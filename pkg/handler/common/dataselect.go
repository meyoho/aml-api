package common

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/dataselect"
	"fmt"
	"github.com/emicklei/go-restful"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
	"strconv"
	"strings"
)

// PropertyName is used to get the value of certain property of data cell.
// For example if we want to get the namespace of certain Deployment we can use DeploymentCell.GetProperty(NamespaceProperty)
type PropertyName string

// List of all property names supported by the UI.
const (
	NameProperty                       = "name"
	NameLengthProperty                 = "nameLength"
	CreationTimestampProperty          = "creationTimestamp"
	NamespaceProperty                  = "namespace"
	StatusProperty                     = "status"
	ScopeProperty                      = "scope"
	KindProperty                       = "kind"
	DisplayNameProperty                = "displayName"
	DisplayEnNameProperty              = "displayEnName"
	DisplayZhNameProperty              = "displayZhName"
	DomainProperty                     = "domain"
	LabelProperty                      = "label"
	SecretTypeProperty                 = "secretType"
	ProjectProperty                    = "project"
	ProductNameProperty                = "productName"
	PipelineConfigProperty             = "pipelineConfig"
	CodeRepoServiceProperty            = "codeRepoService"
	CodeRepoBindingProperty            = "codeRepoBinding"
	CodeRepositoryProperty             = "codeRepository"
	CodeQualityBindingProperty         = "codeQualityBinding"
	ExactNameProperty                  = "exactName"
	LabelEqualProperty                 = "labelEq"
	JenkinsProperty                    = "jenkins"
	JenkinsBindingProperty             = "jenkinsBinding"
	StartedAtProperty                  = "startedAt"
	PipelineCreationTimestampProperty  = "pipelineCreationTimestamp"
	ImageRegistryProperty              = "imageRegistry"
	ImageRegistryBindingProperty       = "imageRegistryBinding"
	ImageRepositoryProperty            = "imageRepository"
	LatestCommitAt                     = "latestCommitAt"
	MicroservicesConfigProfileProperty = "profile"
	MicroservicesConfigLabelProperty   = "label"
	CategoryProperty                   = "category"
	MultiBranchCategoryProperty        = "multiBranchCategory"
	MultiBranchNameProperty            = "multiBranchName"
	ASMHostName                        = "asmHost"
)

// Parses query parameters of the request and returns a DataSelectQuery object
func ParseDataSelectPathParameter(request *restful.Request) *dataselect.Query {
	paginationQuery := parsePaginationPathParameter(request)
	sortQuery := parseSortPathParameter(request)
	filterQuery := parseFilterPathParameter(request)
	return FilterByDisplaynameToLower(dataselect.NewDataSelectQuery(paginationQuery, sortQuery, filterQuery))
}

func parsePaginationPathParameter(request *restful.Request) *dataselect.PaginationQuery {
	itemsPerPage, err := strconv.ParseInt(request.QueryParameter("itemsPerPage"), 10, 0)
	if err != nil {
		return dataselect.NoPagination
	}

	page, err := strconv.ParseInt(request.QueryParameter("page"), 10, 0)
	if err != nil {
		return dataselect.NoPagination
	}

	// Frontend pages start from 1 and backend starts from 0
	return dataselect.NewPaginationQuery(int(itemsPerPage), int(page-1))
}

// Parses query parameters of the request and returns a SortQuery object
func parseSortPathParameter(request *restful.Request) *dataselect.SortQuery {
	return dataselect.NewSortQuery(strings.Split(request.QueryParameter("sortBy"), ","))
}

func parseFilterPathParameter(request *restful.Request) *dataselect.FilterQuery {
	return dataselect.NewFilterQuery(strings.Split(request.QueryParameter("filterBy"), ","))
}

func FilterByDisplaynameToLower(query *dataselect.Query) *dataselect.Query {
	if query == nil || query.FilterQuery == nil || len(query.FilterQuery.FilterByList) == 0 {
		return query
	}
	for i, f := range query.FilterQuery.FilterByList {
		if f.Property == DisplayNameProperty {
			f.Value = dataselect.StdComparableString(strings.ToLower(fmt.Sprintf("%s", f.Value)))
			query.FilterQuery.FilterByList[i] = f
			break
		}
	}
	return query
}

func GetLabelSelectorByDsQuery(dsQuery *dataselect.Query) labels.Selector {
	labelSelector := labels.NewSelector()
	if dsQuery == nil || dsQuery.FilterQuery == nil || len(dsQuery.FilterQuery.FilterByList) == 0 {
		return labelSelector
	}

	for _, filterBy := range dsQuery.FilterQuery.FilterByList {
		if filterBy.Property != "label" {
			continue
		}

		filterStrs := strings.Split(fmt.Sprintf("%s", filterBy.Value), ",")
		for _, filterStr := range filterStrs {
			keyAndValue := strings.Split(filterStr, ":")
			if len(keyAndValue) == 2 {
				req, _ := labels.NewRequirement(keyAndValue[0], selection.DoubleEquals, []string{keyAndValue[1]})
				labelSelector = labelSelector.Add(*req)
			}
		}
	}

	return labelSelector
}

// ListMeta describes list of objects, i.e. holds information about pagination options set for the list.
type ListMeta struct {
	// Total number of items on the list. Used for pagination.
	TotalItems int `json:"totalItems"`
}
