package context

import (
	"alauda.io/amlopr/pkg/client/clientset/versioned"
	"context"
	pyTorchjob "github.com/kubeflow/pytorch-operator/pkg/client/clientset/versioned"
	tfjob "github.com/kubeflow/tf-operator/pkg/client/clientset/versioned"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// Context simple pointer to context.Context interface
type Context context.Context

type contextKey struct{ Name string }

var (
	amlClientKey           = contextKey{Name: "aml.Interface"}
	amlPytorchClientKey    = contextKey{Name: "amlpytorch.Interface"}
	amlTensorflowClientKey = contextKey{Name: "amltensorflow.Interface"}
	devopsClientKey        = contextKey{Name: "devops.Interface"}
	kubernetesClientKey    = contextKey{Name: "kubernetes.Interface"}
	appclientKey           = contextKey{Name: "appcore.Interface"}
	dynamicClientKey       = contextKey{Name: "dynamic.NamespaceableResourceInterface"}
	loggerKey              = contextKey{Name: "zap.Logger"}
	dataselectQueryKey     = contextKey{Name: "dataselect.Query"}
	configKey              = contextKey{Name: "config"}
)

type mlContext struct {
	origin context.Context
	keys   []interface{}
	values []interface{}
}

func NewMLContext(origin context.Context) *mlContext {
	return &mlContext{origin: origin}
}

func (c *mlContext) InjectClient(key interface{}, value interface{}) *mlContext {
	c.keys = append(c.keys, key)
	c.values = append(c.values, value)
	return c
}

func (c *mlContext) GetContext() context.Context {
	result := c.origin
	for i := range c.keys {
		result = context.WithValue(result, c.keys[i], c.values[i])
	}
	return result
}

// WithDevOpsClient inserts a client into the context
func WithDevOpsClient(ctx context.Context, client versioned.Interface) context.Context {
	return context.WithValue(ctx, devopsClientKey, client)
}

// InjectAMLClient inserts a client into the context
func (c *mlContext) InjectAMLClient(client versioned.Interface) *mlContext {
	return c.InjectClient(amlClientKey, client)
}
func (c *mlContext) InjectPytorchClient(client pyTorchjob.Interface) *mlContext {
	return c.InjectClient(amlPytorchClientKey, client)
}
func (c *mlContext) InjectTensorflowClient(client tfjob.Interface) *mlContext {
	return c.InjectClient(amlTensorflowClientKey, client)
}
func (c *mlContext) InjectK8sClient(client kubernetes.Interface) *mlContext {
	return c.InjectClient(kubernetesClientKey, client)
}

//func WithAppClient(ctx context.Context, client *app.ApplicationClient) context.Context {
//	return context.WithValue(ctx, appclientKey, client)
//}

func WithConfig(ctx context.Context, config *rest.Config) context.Context {
	return context.WithValue(ctx, configKey, config)
}

// AMLClient fetches a client from a context if existing.
// will return nil if the context doesnot have the client
func AMLClient(ctx context.Context) versioned.Interface {
	val := ctx.Value(amlClientKey)
	if val != nil {
		return val.(versioned.Interface)
	}
	return nil
}
func PytorchClient(ctx context.Context) pyTorchjob.Interface {
	val := ctx.Value(amlPytorchClientKey)
	if val != nil {
		return val.(pyTorchjob.Interface)
	}
	return nil
}
func TensorflowClient(ctx context.Context) tfjob.Interface {
	val := ctx.Value(amlTensorflowClientKey)
	if val != nil {
		return val.(tfjob.Interface)
	}
	return nil
}

// InsecureClient fetches a client from a context if existing.
// will return nil if the context doesnot have the client
func InsecureClient(ctx context.Context) kubernetes.Interface {
	val := ctx.Value(kubernetesClientKey)

	if val != nil {
		return val.(kubernetes.Interface)
	}
	return nil
}

//func AppClient(ctx context.Context) *app.ApplicationClient {
//	val := ctx.Value(appclientKey)
//	if val != nil {
//		return val.(*app.ApplicationClient)
//	}
//	return nil
//}

func SecureClient(ctx context.Context) kubernetes.Interface {
	val := ctx.Value(kubernetesClientKey)
	if val != nil {
		return val.(kubernetes.Interface)
	}
	return nil
}

func ConfigClient(ctx context.Context) *rest.Config {
	val := ctx.Value(configKey)
	if val != nil {
		return val.(*rest.Config)
	}
	return nil
}
