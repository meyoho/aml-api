package base

import "alauda.io/aml-api/pkg/handler/common/env"

func GetAlaudaNamespace() string {
	return env.GetEnv(env.Namespace, "alauda-system")
}
