package decorator

import (
	"alauda.io/aml-api/pkg/context"
	mlclientset "alauda.io/amlopr/pkg/client/clientset/versioned"
	abdecorator "bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/emicklei/go-restful"
	pyTorchjob "github.com/kubeflow/pytorch-operator/pkg/client/clientset/versioned"
	tfjob "github.com/kubeflow/tf-operator/pkg/client/clientset/versioned"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"sync"
)

var (
	initClient sync.Once
	clientGen  Client
)

func ClientDecorator(svr server.Server) Client {
	initClient.Do(func() {
		clientGen = NewClient(svr)
	})
	return clientGen
}

type Client struct {
	abdecorator.Client
}

func NewClient(svr server.Server) Client {
	return Client{
		Client: abdecorator.Client{
			Server: svr,
		},
	}
}

type AMlClientManger struct {
	mlClient      mlclientset.Interface
	tfClient      tfjob.Interface
	pytorchClient pyTorchjob.Interface
	k8sClient     kubernetes.Interface
}

func (d Client) AMLClientFilter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	config, err := d.Client.GetManager().Config(req)
	if err != nil {
		d.Client.Server.HandleError(err, req, resp)
		return
	}
	mlclient, err := mlclientset.NewForConfig(config)
	if err != nil {
		d.Client.Server.HandleError(err, req, resp)
		return
	}
	// add torch-client
	torchClient, err := pyTorchjob.NewForConfig(config)
	if err != nil {
		d.Client.Server.HandleError(err, req, resp)
		return
	}
	// add tf-client
	tfClient, err := tfjob.NewForConfig(config)
	if err != nil {
		d.Client.Server.HandleError(err, req, resp)
		return
	}

	k8sClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		d.Client.Server.HandleError(err, req, resp)
		return
	}

	amlClientMgr := AMlClientManger{
		mlClient:      mlclient,
		tfClient:      tfClient,
		pytorchClient: torchClient,
		k8sClient:     k8sClient,
	}
	d.SetAMLClientCtx(amlClientMgr, err, req, resp, chain)
}

func (d Client) SetAMLClientCtx(clientMgr AMlClientManger, err error, req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	if err != nil /*|| client == nil*/ {
		d.Client.Server.HandleError(err, req, resp)
		return
	}
	ctx := context.NewMLContext(req.Request.Context()).
		InjectAMLClient(clientMgr.mlClient).
		InjectPytorchClient(clientMgr.pytorchClient).
		InjectTensorflowClient(clientMgr.tfClient).
		InjectK8sClient(clientMgr.k8sClient).
		GetContext()

	req.Request = req.Request.WithContext(ctx)
	chain.ProcessFilter(req, resp)
}

func (d Client) ConfigFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	// generate config from request
	config, err := d.Client.GetManager().Config(req)
	if err != nil {
		d.Client.Server.HandleError(err, req, res)
		return
	}
	d.SetConfigContext(config, err, req, res, chain)
}

func (d Client) SetConfigContext(config *rest.Config, err error, req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	if err != nil || config == nil {
		d.Client.Server.HandleError(err, req, res)
		return
	}
	req.Request = req.Request.WithContext(context.WithConfig(req.Request.Context(), config))
	chain.ProcessFilter(req, res)
}
