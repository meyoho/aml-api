package decorator

import (
	"sync"

	"bitbucket.org/mathildetech/alauda-backend/pkg/context"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/log"
	restful "github.com/emicklei/go-restful"
	"go.uber.org/zap"
)

var (
	initLogger sync.Once
	loggerGen  Logger
)

// LoggerDecorator decorator for logger generator
func LoggerDecorator(svr server.Server) Logger {
	initLogger.Do(func() {
		loggerGen = NewLogger(svr)
	})
	return loggerGen
}

// Logger logger injector middleware
// will inject a logger instance into the context
type Logger struct {
	Server server.Server
}

// NewLogger constructor for new client
func NewLogger(svr server.Server) Logger {
	return Logger{
		Server: svr,
	}
}

// Filter creates a basic filter adding only request data to the logger
func (l Logger) Filter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	logger := l.GetLogger(req)
	logger = logger.With(log.String("url", req.Request.RequestURI))
	l.SetContext(req, logger)
	chain.ProcessFilter(req, res)
}

// CustomFilter creates a new filter to add specific static context values to the logger
func (l Logger) CustomFilter(fields ...log.Field) restful.FilterFunction {
	return func(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
		logger := l.GetLogger(req)
		logger = logger.With(fields...)
		l.SetContext(req, logger)
		chain.ProcessFilter(req, res)
	}
}

// GetLogger gets a logger from a context if existing, otherwise just inits a new one
func (l Logger) GetLogger(req *restful.Request) (logger *zap.Logger) {
	if req != nil {
		logger = context.Logger(req.Request.Context())
	}
	if logger == nil {
		logger = l.Server.L()
	}
	return
}

// SetContext sets on context
func (l Logger) SetContext(req *restful.Request, logger *zap.Logger) {
	req.Request = req.Request.WithContext(context.WithLogger(req.Request.Context(), logger))
}
