package main

import (
	"alauda.io/aml-api/cmd/aml-api/app"
	_ "alauda.io/aml-api/pkg/handler"
	"math/rand"
	"os"
	"runtime"
	"time"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		runtime.GOMAXPROCS(runtime.NumCPU())
	}
	app.NewApp("aml-api").Run()
}
