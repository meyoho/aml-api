package options

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var MlOptions = &MLOptions{
	MorgansHost: "http://morgans:8080",
}

type MLOptions struct {
	MorgansHost string
}

const (
	configMorgansHost = "morgansHost"
)

func (o *MLOptions) AddFlags(fs *pflag.FlagSet) {
	fs.String("morgans-host", o.MorgansHost,
		"Multi cluster host full fledged address.")
	_ = viper.BindPFlag(configMorgansHost, fs.Lookup("morgans-host"))
}

func (o *MLOptions) ApplyFlags() []error {
	o.MorgansHost = viper.GetString(configMorgansHost)
	return nil
}

func (o *MLOptions) ApplyToServer(server.Server) error {
	return nil
}
