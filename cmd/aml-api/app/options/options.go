package options

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
	"flag"
	"github.com/spf13/pflag"
	"k8s.io/klog"
)

// Options options for archon-api
type Options struct {
	options.Optioner
}

// NewOptions new options for archon-api
func NewOptions(name string) *Options {
	// recommended := options.NewRecommendedOptions().Options
	return &Options{
		//Optioner: options.NewRecommendedOptions(),
		Optioner: options.With(
			options.NewLogOptions(),
			NewKlogOptions(),
			options.NewInsecureServingOptions(),
			options.NewClientOptions(),
			options.NewDebugOptions(),
			options.NewMetricsOptions(),
			options.NewAPIRegistryOptions(),
			options.NewOpenAPIOptions(),
			options.NewErrorOptions(),
			MlOptions,
		),
	}
}

// KlogOptions options for klog
type KlogOptions struct {
}

var _ options.Optioner = &KlogOptions{}

// NewKlogOptions constructs klog options
func NewKlogOptions() *KlogOptions {
	return &KlogOptions{}
}

// AddFlags add flags to this option
func (o *KlogOptions) AddFlags(fs *pflag.FlagSet) {
	flagSet := flag.NewFlagSet("mlLog", flag.ExitOnError)
	klog.InitFlags(flagSet)
	fs.AddGoFlagSet(flagSet)
}

// ApplyFlags parsing parameters from the command line or configuration file
// to the options instance.
func (o *KlogOptions) ApplyFlags() []error {
	return nil
}

// ApplyToServer apply options on server
func (o *KlogOptions) ApplyToServer(srv server.Server) error {
	return nil
}
