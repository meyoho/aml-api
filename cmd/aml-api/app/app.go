package app

import (
	"alauda.io/aml-api/cmd/aml-api/app/options"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/app"
)

const commandDesc = `This application is an API sever of Alauda Machine Learning.`

// NewApp creates a new archon-api app
func NewApp(name string) *app.App {
	opts := options.NewOptions(name)
	application := app.NewApp("AML API Server",
		name,
		app.WithOptions(opts),
		app.WithDescription(commandDesc),
		app.WithRunFunc(run(opts)),
	)
	return application
}

func run(opts *options.Options) app.RunFunc {
	return func(basename string) error {
		srv := server.New(basename)
		err := opts.ApplyToServer(srv)
		if err != nil {
			return err
		}
		srv.Start()
		return nil
	}
}
