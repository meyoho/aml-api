module alauda.io/aml-api

go 1.12

replace (
	alauda.io/amlopr => bitbucket.org/mathildetech/amlopr v2.5.1-0.20191212104152-b243bf1df67f+incompatible
	alauda.io/devops-apiserver => bitbucket.org/mathildetech/devops-apiserver v1.8.0
	github.com/appscode/jsonpatch => gomodules.xyz/jsonpatch/v2 v2.0.0-20190626003512-87910169748d
	k8s.io/api => k8s.io/api v0.0.0-20190313235455-40a48860b5ab
	k8s.io/apimachinery => k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1
)

require (
	alauda.io/amlopr v0.0.0-20190916025207-41a1b2cc71c5
	alauda.io/devops-apiserver v1.8.0 // indirect
	bitbucket.org/mathildetech/alauda-backend v0.1.18
	bitbucket.org/mathildetech/app v1.0.1
	bitbucket.org/mathildetech/log v1.0.5
	github.com/appscode/jsonpatch v0.0.0-00010101000000-000000000000 // indirect
	github.com/emicklei/go-restful v2.9.6+incompatible
	github.com/go-logr/logr v0.1.0 // indirect
	github.com/go-logr/zapr v0.1.1 // indirect
	github.com/google/go-cmp v0.3.0
	github.com/juju/errors v0.0.0-20181118221551-089d3ea4e4d5
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20190723135506-ce30eb24acd2 // indirect
	github.com/kubeflow/pytorch-operator v0.5.1
	github.com/kubeflow/tf-operator v0.5.1
	github.com/kubernetes-sigs/application v0.8.1
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/prometheus/client_golang v0.9.3-0.20190127221311-3c4408c8b829
	github.com/prometheus/common v0.2.0
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.3.2
	go.uber.org/zap v1.9.1
	gomodules.xyz/jsonpatch/v2 v2.0.1 // indirect
	gopkg.in/mgo.v2 v2.0.0-20180705113604-9856a29383ce // indirect
	k8s.io/api v0.0.0-20190731142925-739c7f7721ed
	k8s.io/apiextensions-apiserver v0.0.0-20190731184107-91cdd51c95ef // indirect
	k8s.io/apimachinery v0.0.0-20190731142807-035e418f1ad9
	k8s.io/client-go v11.0.0+incompatible
	k8s.io/klog v0.3.3
	sigs.k8s.io/controller-runtime v0.1.12
	sigs.k8s.io/testing_frameworks v0.1.1 // indirect
	sigs.k8s.io/yaml v1.1.0
)
