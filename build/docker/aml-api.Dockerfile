FROM alpine:latest

ENV TZ=Asia/Shanghai

WORKDIR /aml

CMD ["/aml/api"]

ENTRYPOINT ["/bin/sh", "-c", "/aml/api \"$0\" \"$@\""]

ADD output/linux/amd64/aml-api /aml/api